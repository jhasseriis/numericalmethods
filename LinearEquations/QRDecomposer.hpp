#pragma once

#include <armadillo>
#include <iostream>
#include <math.h>

using namespace arma;
using namespace std;

class QRDecomposer{

public:
	
	virtual void decomp(mat& A,mat& R)= 0;
	virtual void solve(mat& Q, mat& R, vec& b, vec& x)= 0;
	virtual	double absdet(mat& R)= 0;
	virtual void invert(mat& Q, mat& R,mat& Ainverse)=0 ;


	void CheckOrtho(mat &Q,double tolerance);	
	void BackSub(mat& A,vec& b);


};
