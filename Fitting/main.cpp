#include <iostream>
#include <armadillo>
#include "LeastSquareFitter.hpp"

using namespace std;
using namespace arma;

using fi=std::function<double(double)>; // function reference  alias
using fN = std::vector<fi>; // vector of function references alias

// fit functions
double f1(double x) { return 1.0/x;}
double f2(double x) { return 1.0;}
double f3(double x) { return x; }

int main(){

	
	fN funcList;
	funcList.push_back(f1);	
	funcList.push_back(f2);
	funcList.push_back(f3);

/*
	for(unsigned int i = 0 ; i < funcList.size(); i++){	
		cout <<	funcList[i](2) << endl;
	}	
*/	
	
	vec x,y,dy;
	x <<  0.100  << 0.145 <<  0.211  << 0.307  << 0.447  << 0.649 <<   0.944 <<  1.372<<   1.995  << 2.900;
	y <<  12.189<<   9.379<<   7.475  <<  6.567 <<  5.563<<   5.337 <<  5.941<<   6.740<<   8.663<<  10.792;
	dy<<   0.857 <<  0.532  << 0.403  << 1.078<<   0.494 <<  0.596<<   0.463 <<  0.198 <<  0.754<<   0.829;


//	x = linspace<vec>(0.1,5,10);
//	y = x;
//	dy.fill(0.001);

	LeastSquareFitter lsfit(funcList,x,y,dy); 
	lsfit.evalFit();
	lsfit.fitSave();		
	


	return 0;
}
