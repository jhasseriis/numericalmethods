// mainCubic.cpp
#include <iostream>
#include "CubicInterpolator.hpp"
#include <cmath>
#include <fstream>

using namespace arma;
using namespace std;

int main(){
	vec xi = linspace<vec>(0,2*3.1415,52);
	vec yi(size(xi));
	
	for(int i=0; i<xi.n_elem;i++){
		yi(i) = sin(xi(i)); 
//		yi(i) = xi(i)*xi(i);
//		yi(i) = xi(i);
//		yi(i) = 1;
	}	
	
	CubicInterpolator cubicinterp(xi,yi);
	
	vec sampleXi=linspace<vec>(0,2*3.1415,500);

	cubicinterp.exportToTxt(sampleXi);	
	
	return 0;	
}
