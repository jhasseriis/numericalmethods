#pragma once

#include <iostream>
#include <armadillo>
#include <cmath>

using namespace std;
using namespace arma;

using func = std::function<double(vec)>;

class MonteCarlo{
public:

	MonteCarlo(){arma_rng::set_seed_random();}; // set seeding to random for createXsample method

	// methods
	void createXsample();
	void SetSystem(func function,vec a,vec b);
	double PlainIntegrate(func function, vec a, vec b, int N,double* err);
	double PlainIntegrateWithTolerance(func function,vec a, vec b, double tol,  double* err);

	// variables
	int dim;
	double N,V;
	double avg,var,err, Q;
	vec a,b; // volume boundaries
	vec x;
	vec rndVect;
	func function;

};
