# Interpolation plot
# set terminal aqua
set terminal pdf
set output 'output/Interpolation.pdf'
set xlabel 'x'
set ylabel 'y'
f(x,y) = sin(x)*exp(-y*y)

splot 'output/InterpolationGrid.txt' using 1:2:($3*-3)  title 'Grid points', 'output/Interpolation.txt' title 'Interpolation of: sin(x)*exp(-y^2)'  with lines, f(x,y) title 'Exact function: sin(x)*exp(-y^2)' lt rgb "red"
