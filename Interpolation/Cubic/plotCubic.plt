set term aqua
set key outside 
set title "Cubic interpolation"
#set xrange [0:3]
#set yrange [0:2]

plot "CubicInterpolator.txt" index 0 title 'Tabulated points',\
"CubicInterpolator.txt" index 1 with lines title 'Interpolated points',\
"CubicInterpolator.txt" index 1 using 1:3 with lines title 'Derivative',\
"CubicInterpolator.txt" index 1 using 1:($4-1) with lines title 'Integral',\
-cos(x) title '-cos(x)'
#x**3/3 title 'x^3/3'
