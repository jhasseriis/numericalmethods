#include "Driver.hpp"

Driver::Driver(double a, double b, double h0, double acc, double eps, vec y0,
		void f(double t, vec& y, vec& dydt),
		void stepper(double t, double h, vec& y, void f(double t, vec& y, vec& dydt),vec& yh,vec& err,int* n_evals)) : 
	h(h0),t(a),a(a),b(b),acc(acc),eps(eps),y(y0), f(f),stepper(stepper){
		n = y.n_elem;	
		err = zeros<vec>(n);
		yh = zeros<vec>(n);
		dydt = zeros<vec>(n);
	}

void Driver::Solve(){
	n_evals = 0; 
	while(t < b){
//		cout << "t: " << t << ",  h: " << h << endl;
		Drive();
		AdaptSize();
	}
	cout << "# RHS evals from a->b: " << n_evals << endl; 
}

void Driver::Drive(){
	stepper(t,h,y,f,yh,err,&n_evals);
}

void Driver::AdaptSize(){
	double Power = 0.25;
	double Safety = 0.95;
	ei = norm(err);
	ti = (eps*norm(y)+acc)*sqrt(h/(b-a));

	if(ei<ti){
		ts.push_back(t);
		ys.push_back(y);

		t = t+h;
		y = yh;	

	}
	if(ei > 0) 	h = h * pow((ti/ei),Power)*Safety;
	else h=2*h;
}

void Driver::PrintPath(){
	for(int i = 0; i < ts.size(); i++){
		cout << "t=" << ts[i] << "\t" << "y="; 
		for(int j=0; j < n;j++){
			cout << ys[i].at(j) << "\t"; 
		}
		cout << endl;
	}
}

void Driver::WritePath(){
	ofstream myfile;
	myfile.open("Results.txt");
	myfile << "t \t y[0] \t y[1]" << endl;
	for(int i = 0; i < ts.size(); i++){
		myfile  << ts[i] << "\t" ; 
		for(int j=0; j < n;j++){
			myfile << ys[i].at(j) << "\t"; 
		}
		myfile << endl;
	}
	myfile.close();
}
