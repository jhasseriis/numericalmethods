#include "CubicInterpolator.hpp"

// The constructor builds pi and ci; then you can find  any S(x) between the boundary  x values !
CubicInterpolator::CubicInterpolator(vec xi,vec yi):
	xi(xi), yi(yi), pi(xi.n_elem-1), ci(xi.n_elem-1),SiIntegrals(xi.n_elem-1),di(xi.n_elem-1),bi(xi.n_elem)
	// note that bi is a size larger because it needs the auxillary coefficient
{
	// There are n-1 'line' segments
	double dx,dy;
	vec dxi(xi.n_elem-1);	// Calculate pi(i)
	for(int i=0; i < xi.n_elem-1; i++){
		dx=xi(i+1)-xi(i);
		dy=yi(i+1)-yi(i);
		pi(i) = dy/dx;
		dxi(i) = dx;
	}			


	// now we have pi, dxi=hi
	// Calculation of bi:
	// helper vectors:
	vec D(bi.n_elem); D(0) = 2; D(D.n_elem-1) = 2;
	vec Q(bi.n_elem-1); Q(0) = 1;
	vec B(bi.n_elem); B(0) = 3*pi(0); B(B.n_elem-1) = 3*pi(pi.n_elem-2); 
	cout << endl <<  "Setting up tridiag. system, Gauss eliminating and perform backsubstitution..." ;

	for(int i = 0; i < D.n_elem-2; i++){
		D(i+1) = 2.0*dxi(i)/dxi(i+1) + 2;	
		B(i+1) = 3*(pi(i)+pi(i+1)*dxi(i)/dxi(i+1));
		Q(i+1) = dxi(i)/dxi(i+1);
	}
   	   
	// more helper vectors:
	vec Dt(D.n_elem); Dt(0) = D(0);
	vec Bt(B.n_elem); Bt(0) = B(0);	
	for(int i=1;i < D.n_elem; i++){
		Dt(i) = D(i)-Q(i-1)/Dt(i-1);
		Bt(i) = B(i)-Bt(i-1)/Dt(i-1);
	}
	// backsubstitution for bi
	bi(bi.n_elem-1) = Bt(bi.n_elem-1)/Dt(bi.n_elem-1);
	for(int i=bi.n_elem-2; i >= 0;i--){
		bi(i) = (Bt(i)-Q(i)*bi(i+1))/Dt(i);
	}
	cout << "DONE!" << endl << "Solving for remaining coefficients...";
	// finally ci and di:
	for(int i=0; i<ci.n_elem;i++){
		ci(i) = (-2.0*bi(i)-bi(i+1)+3.0*pi(i))/dxi(i);
		di(i) = ((bi(i)+bi(i+1)-2.0*pi(i))/pow(dxi(i),2));
	}
	cout << "DONE!" << endl << endl;

	

	double lower, upper;
	// Calculation of integration over each Si segment
	double sum=0;
	for(int i=0;i<SiIntegrals.n_elem;i++){
	
		lower =	
			yi(i)*xi(i)-bi(i)*pow(xi(i),2)/2.0;

		upper =
			yi(i)*xi(i+1) -
			bi(i)*xi(i)*xi(i+1) +
			bi(i)*pow(xi(i+1),2)/2.0 +
			1/3.0*ci(i)*pow(xi(i+1)-xi(i),3) +
			1/4.0*di(i)*pow(xi(i+1)-xi(i),4);
		
		SiIntegrals(i) = upper - lower;
	 }

};

CubicInterpolator::~CubicInterpolator(){};

double CubicInterpolator::derivative(double z){
	return evaluate(z,"Derivative");
}

double CubicInterpolator::integrate(double z){
	return evaluate(z,"Integrate");
}

double CubicInterpolator::evaluate(double z,string type=""){	
	vec subArray = xi;
	vec tempArray;
	
	double midElemVal;  
	double midElemIndex;

	while(subArray.n_elem > 2){
		midElemIndex = floor(subArray.n_elem/2);
		midElemVal = subArray(midElemIndex);
			if(midElemVal > z){
				subArray = splitArray(subArray,0,midElemIndex);
			}
			else{
				subArray = splitArray(subArray,midElemIndex,subArray.n_elem-1);
			}
	}
	double lowerIntervalValue = subArray(0); // there are only 2 elements left
	int index = findIndex(xi,lowerIntervalValue); // find the correct index

	//cout << "yi(index) : " << yi(index) << endl;
	//cout << xi(index) << " < " << z << " < " << xi(index+1) << endl;
	

	double result=0;
	if(type==""){ // if simply evaluating Si	
		result =yi(index)+
				bi(index)*pow(z-xi(index),1)+
				ci(index)*pow(z-xi(index),2)+
				di(index)*pow(z-xi(index),3);	
	}

	
	if(type=="Derivative"){
		result = bi(index)+2*ci(index)*(z-xi(index))+3*di(index)*pow(z-xi(index),2); 
	}
	
	if(type=="Integrate"){
		// First sum contributions from x1 to xi(index)-1 
		for(int i=0; i<index-1;i++){	
			result = result + SiIntegrals(i);
		}
		// integrate from xi(index) to z	
		double lower =	
			yi(index)*xi(index) -
			bi(index)*pow(xi(index),2)/2.0;

		double upper =  
			yi(index)*z -
			bi(index)*xi(index)*z +
			bi(index)*pow(z,2)/2.0 +
			1/3.0*ci(index)*pow(z-xi(index),3) +
			1/4.0*di(index)*pow(z-xi(index),4);

		result = result + upper - lower;
	}

	return result;
};


// This function splits an array from lowerIndex to upperIndex and returns it in a new array
vec CubicInterpolator::splitArray(vec array,int lowerIndex,int upperIndex){
	int arrayLength = (upperIndex-lowerIndex)+1;
	vec returnArray(arrayLength);	
	int newIndex=0;

	for(int i=0;i<array.n_elem;i++){
		if(i>=lowerIndex && i<=upperIndex){
			returnArray(newIndex) = array(i);
			newIndex++;
		}
	}
	return returnArray;
}


void CubicInterpolator::exportToTxt(vec x){
	ofstream file;
	file.open(fileWriteName);
	cout << "Writing to file : " << fileWriteName << endl;		
//	file << endl << "# Tabulated points" << endl;

	for(int i=0; i<xi.n_elem;i++){
		file << xi(i) << "\t" << yi(i)<< endl;	
	}

//	file << endl << "# Interpolated points" << endl;
	file << endl << endl;;
	for(int i=0; i<x.n_elem;i++){
		file  << x(i) << "\t"  << 
			evaluate(x(i)) << "\t" << 
			derivative(x(i)) << "\t" << 
			integrate(x(i)) <<
			endl;
	}
	file.close();
}


int CubicInterpolator::findIndex(vec array,double value){

	for(int i=0; i<array.n_elem; i++){
		if(array(i) == value) return i;	
	}
	cout << "ERROR: No value " << value << " in this array! Returning -1";
	return -1; 
}



