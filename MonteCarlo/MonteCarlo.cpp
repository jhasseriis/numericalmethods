#include "MonteCarlo.hpp"


double MonteCarlo::PlainIntegrate(func function,vec a, vec b, int N, double* err){
	SetSystem(function,a,b);
	
	double sum = 0, sum_of_squares = 0, fx = 0;

	for(int i=0; i<N;i++){
		createXsample();
		fx = function(x);
		sum = sum + fx;
		sum_of_squares = sum_of_squares + fx*fx;	
	}

	avg = sum/N; // avg
	var = sum_of_squares/N-avg*avg; // variance
	this->err = sqrt(var/N)*V; // sigma, std
	Q = V*avg; // result

	if(err!=NULL) *err = this->err; // if pointer to error value is given
	return Q;
}

double MonteCarlo::PlainIntegrateWithTolerance(func function, vec a, vec b, double tol,double* err){
	SetSystem(function,a,b);

	
	double sum = 0, sum_of_squares = 0, fx = 0;
	N = 2;
	do{
		createXsample();
		fx = function(x);
		sum = sum + fx;
		sum_of_squares = sum_of_squares + fx*fx;	

		avg = sum/N;
		var = sum_of_squares/N-avg*avg;
		*err = sqrt(var/N)*V;
		
		//cout << "N : " << N << ",  err: " << *err <<  endl;
		N++;
	}while(*err > tol);
	this->err = *err;	
	Q = V*avg;
	return Q;
	

}



void MonteCarlo::createXsample(){	
	rndVect	= randu<vec>(dim);
	x = a + rndVect%(b-a);
}

void MonteCarlo::SetSystem(func function, vec a, vec b){
	arma_rng::set_seed_random();
	this->function = function;
	this->a = a;
	this->b = b;
	this->dim = b.n_elem;
	this->x = zeros<vec>(this->dim);
	
	this->V=b(0)-a(0);
	for(int i=1; i < this->dim;i++) V = V*(b(i)-a(i));
}


