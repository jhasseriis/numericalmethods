#include "Minimizer.hpp"

Minimizer::Minimizer(f function, fi gradientFuncs, fij hessianFuncs, vec xInitial):
	function(function), gradientFuncs(gradientFuncs), hessianFuncs(hessianFuncs), x(xInitial){

	n = hessianFuncs.size();		
	hessianInverse = zeros<mat>(n,n);	hessianInverse.eye();
	hessian= zeros<mat>(n,n);	
	gradient = zeros<vec>(n);

	firstHessianEvaluation = true;
}

void Minimizer::SetSystem(f function, fi gradientFuncs, fij hessianFuncs, vec xInitial){

	this->function = function;
	this->gradientFuncs = gradientFuncs;
	this->hessianFuncs = hessianFuncs;
	this->x = xInitial;

	n = hessianFuncs.size();		
	hessianInverse = zeros<mat>(n,n);	hessianInverse.eye();
	hessian= zeros<mat>(n,n);	
	gradient = zeros<vec>(n);
	
	firstHessianEvaluation = true;
}


void Minimizer::Minimize(){
	CalculateGradInPlace();	
	//cout << "grad: " << trans(gradient) << endl;
	int stepCount = 0;

	while(norm(gradient) > accuracy){
		FindStepInPlace();	
		Backtrack();	
//		cout << "norm(gradient) : " << norm(gradient) << endl;
		stepCount++;
	}
	cout << "found extremum at (" << x(0) << "," << x(1) << ")" << " after " << stepCount<< " steps!" <<  endl;
}



void Minimizer::Backtrack(){
	double lambda = 1;

	vec xPlusLambdadx; 

	while(lambda >= lambdaMin){
		xPlusLambdadx = x + lambda*dx;
		if(function(xPlusLambdadx) < function(x) + alpha*lambda*dot(trans(dx),gradient)) break;	 // break while loop if the step is smaller
		lambda /= 2.0;
	}
//	cout << "lambda : " << lambda << endl;
//	x = xPlusLambdadx;
	x = x + dx;
}


void Minimizer::FindStepInPlace(){
	CalculateGradInPlace();
	//CalculateHessianInverseInPlace();
	
	//cout << "dx: " << trans(dx) << endl;
	//cout << "hessianinv: " << endl << hessianInverse << endl;
	(*this.*HessianInverseStrategy)(); // calculate hessian inverse by newton or broyden
	dx = -hessianInverse*gradient;
	//cout << "dx = " << trans(dx) ;
	//cout << "grad = " << trans(gradient) << " | " ;
}


vec Minimizer::CalculateGrad(vec& xIn){
	vec grad(n);
	for(int i=0; i < n; i++) grad(i) = gradientFuncs[i](xIn);
	return grad;
}

void Minimizer::CalculateGradInPlace(){	
	for(int i=0; i < n; i++) gradient(i) = gradientFuncs[i](x);
}

void Minimizer::CalculateHessianInverseInPlace(){

	for(int i=0; i < n; i++){
		for(int j=0; j < n; j++){
//			cout << "i,j = " << i << "," << j << endl;
			hessian(i,j) = hessianFuncs[i][j](x); // find hessian 
		}
	}
	
	// We actually need H^-1 for the updates
	auxillaryMatrix = zeros<mat>(n,n);
	solver.decomp(hessian,auxillaryMatrix); // QR decompose the hessian, Q->hessian, R->R 
	solver.invert(hessian,auxillaryMatrix,hessianInverse);


	//cout << "inv(H)*H = "<< endl << hessianInverse*(hessian*auxillaryMatrix) << endl;
}	

void Minimizer::BroydensUpdate(){
	if(firstHessianEvaluation) {
		firstHessianEvaluation = false;
		return;
	}

	vec xPlusDx = x+dx;
	y = CalculateGrad(xPlusDx)-CalculateGrad(x);
	//cout << "y = " << trans(y) << "\t, dx = " << trans(dx) << endl;
	
	double divisor =  dot(y,hessianInverse*dx); // trans(y)*hessianInversex
	mat update = ((dx-hessianInverse*y)*trans(dx)*hessianInverse)/divisor;
	//cout << "update: " << update << endl;
//	cout << "divisor: " << divisor << endl << endl;
	hessianInverse = hessianInverse + update;
}

void Minimizer::SetHessianInverseStrategyToNewton(){
	HessianInverseStrategy = &Minimizer::CalculateHessianInverseInPlace;
}

void Minimizer::SetHessianInverseStrategyToBroyden(){
	HessianInverseStrategy= &Minimizer::BroydensUpdate;
}
