#include <iostream>
#include "Givens.hpp"
#include <math.h>


using namespace std;
using namespace arma;

int main(){

	Givens giv;
	mat A(3,3); A.zeros();

	A <<
	1 << 1 << 2 << endr <<
	0 << 1 << 0 << endr <<
	1 << 5 << 1 << endr;

	mat AOriginal = A;

	cout << "A = " << endl << A << endl;
	giv.decomp(A); // second argument is not used
	cout << "QR = (angles stored in lower triangle part of matrix) " << endl << A << endl;

	cout << "det(A) = det(R) = " << giv.absdet(A) << endl;


	vec x;
	vec b(A.n_cols);
	b(0) = 1;
	b(1) = 2;
	b(2) = 2.2;

	cout << endl << "b =\t" << trans(b) << endl;
	cout << "Solving system for x..." << endl;
	giv.solve(A,b,x);
	cout << " x = " << endl << trans(x) << endl;	
	// To check results it is easiest just to recover Q and R
	mat Q(size(A));	Q.zeros();
	mat R(size(A));	R.zeros();

	cout << "Recovering Q and R..." <<endl;
	giv.RecoverQ(A,Q);
	giv.RecoverR(A,R);

	cout << " R = " << endl << R << endl;
	cout << " Q = " << endl << Q << endl;
	//cout << "QT*Q" << trans(Q)*Q;
	//cout << "Q*R" << Q*R;

	cout << "Checking decomposition QRx = " << endl << trans(Q*R*x)  << endl;

//	mat Ainv(size(A)); Ainv.zeros();
//	cout << "Inverting matrix...";
//	giv.invert(Q,R,Ainv);
//	cout << "Check: inv(A)*A = " << endl << Ainv*AOriginal;

	return 0;
}

