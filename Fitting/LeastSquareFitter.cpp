#include "LeastSquareFitter.hpp"

using fi = std::function<double(double x)>;
using fN = std::vector<fi>;


LeastSquareFitter::LeastSquareFitter(fN& functionList,vec& x,vec& y,vec& dy):
functionList(functionList),x(x),y(y),dy(dy)
{
	evalCoeffs(functionList,x,y,dy);
};

LeastSquareFitter::LeastSquareFitter(){};

void LeastSquareFitter::evalCoeffs(fN& functionList, vec& x,vec& y, vec& dy){
	int n = x.n_elem;
	int m = (int) functionList.size();

	mat A(n,m); //mat R(m,m);

	vec b(n);
	for(int i=0;i<n;i++){
		b(i) = y(i)/dy(i);
	}

	for(int i=0; i<n; i++){
		for(int k=0; k < m;k++){
			A(i,k) = functionList[k](x(i)) / dy(i);
		}
	}

	covariance = inv(trans(A)*A); // evaluate covariance matrix
	//cout << "covar = " << endl << covariance;

	dcoeffs = zeros<vec>(m);
	for(int i=0; i<m ; i++){
		dcoeffs(i) = sqrt(covariance(i,i));
	} 

	//cout << "dcoeffs = " << endl << dcoeffs << endl;

	mat Q(size(A));
	mat R(m,m);
	decomposer.decomp(A,R);	
	decomposer.solve(A,R,b,coeffs); // given Q,R, and RHS b, find c: (QR)c=b
}

void LeastSquareFitter::evalFit(){
	evalFit(x(0),x(x.n_elem-1),500);
}

void LeastSquareFitter::evalFit(double xmin,double xmax,int xsteps){

	xfit = linspace<vec>(xmin,xmax,xsteps);
	yfit = xfit; yfit.zeros();
	yfitPlusdc = yfit;
	yfitMinusdc = yfit;

	double fkxi;
	for(int i=0; i < xsteps; i++){
		for(unsigned int k=0; k < functionList.size(); k++){
			fkxi = functionList[k](xfit(i));
			yfit(i)			+= coeffs(k)*fkxi; // the sum over ck*fk(x)
			yfitPlusdc(i)   += (coeffs(k)+dcoeffs(k))*fkxi; // +/- uncertainty
			yfitMinusdc(i)  += (coeffs(k)-dcoeffs(k))*fkxi;
		}
	}	
}

void LeastSquareFitter::fitSave(){
	fitSave(x(0),x(x.n_elem-1),500);
}

void LeastSquareFitter::fitSave(double xmin,double xmax,int xsteps){
	ofstream myfile;
	myfile.open ("fitResults.txt");
	
	for(int i=0; i<x.n_elem ; i++){
		myfile << x(i) << "\t" << y(i) << "\t" << dy(i) << endl;
	}
	
	myfile << endl << endl;

	for(int i=0; i<xfit.n_elem; i++){
		myfile << xfit(i) << "\t" << yfit(i) << "\t" << yfitPlusdc(i) << "\t" << yfitMinusdc(i) << endl;
	}
		
	myfile.close();
/*
	Gnuplot gp;

	mat result(xfit.n_elem,2);
	result.col(0) = xfit;
	result.col(1) = yfit;

	mat data(x.n_elem,2);
	data.col(0) = x;
	data.col(1) = y;

	gp << "set terminal pdf \n";
	gp << "set output 'test.pdf'\n";
	gp << "plot" << gp.file1d(result) << "with lines title 'cubic',"
				<< gp.file1d(data) << "with points title 'circle'" << std::endl;
	gp << "plot '-' with lines title 'fit',\n";
	gp.send1d(result);
	gp << "'-' with points title 'data'\n";
	gp.send1d(data);
*/
}


