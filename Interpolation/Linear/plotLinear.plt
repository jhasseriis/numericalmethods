set term aqua
set key outside
set title "Linear interpolation"
plot "LinearInterpolator.txt" index 0 title 'Tabulated points',\
"LinearInterpolator.txt" index 1 with lines title 'Interpolated points'
