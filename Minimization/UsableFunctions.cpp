#include "UsableFunctions.hpp"

// Rosenbrock

double Rosenbrock(vec& xvec){
	assert(xvec.n_elem>=2);
	double result,x,y;
	x = xvec(0);
	y = xvec(1);
	result = pow(1-x,2)+pow(y-pow(x,2),2);
	return result;
}

double dxRosenbrock(vec& xvec){return -2*(1-xvec(0))-4*(xvec(1)-pow(xvec(0),2))*xvec(0);}
double dyRosenbrock(vec& xvec){return 2*(xvec(1)-pow(xvec(0),2));}
double dxxRosenbrock(vec& xvec){return 12*pow(xvec(0),2)-4*xvec(1)+2;} 
double dxyRosenbrock(vec& xvec){return -4.0*xvec(0);}
double dyxRosenbrock(vec& xvec){return -4.0*xvec(0);}
double dyyRosenbrock(vec& xvec){return 2.0;}


fi RosenbrockGrad(){
	fi grad;
	grad.push_back(dxRosenbrock);
	grad.push_back(dyRosenbrock);
	return grad;
}
fij RosenbrockHessian(){	
	// creates hessian 'matrix' (vectors in vector)
	fi partials;
	fij hessian	;
	
	partials.push_back(dxxRosenbrock); partials.push_back(dxyRosenbrock);
	hessian.push_back(partials);

	partials.clear();

	partials.push_back(dyxRosenbrock); partials.push_back(dyyRosenbrock);
	hessian.push_back(partials);

	return hessian;
}


// himmelblau gradient
double Himmelblau(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

double dxHimmelblau(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 4.0*x*(pow(x,2)+y-11)+2*(x+pow(y,2)-7);
	return result;
}

double dyHimmelblau(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 2*(pow(x,2)+y-11)+4*y*(x+pow(y,2)-7);
	return result;
}

double dxxHimmelblau(vec& xvec){
	return 4*(pow(xvec(0),2)+xvec(1)-11)+8*pow(xvec(0),2)+2;
}
double dxyHimmelblau(vec& xvec){
	return 4*xvec(0) + 4*xvec(1);
}

double dyxHimmelblau(vec& xvec){
	return 4*xvec(0) + 4*xvec(1);
}

double dyyHimmelblau(vec& xvec){
	return 2 + 4*(xvec(0)+pow(xvec(1),2)-7)+8*pow(xvec(1),2);
}


fi HimmelblauGrad(){
	fi grad;
	grad.push_back(dxHimmelblau);
	grad.push_back(dyHimmelblau);
	return grad;
}
fij HimmelblauHessian(){	
	// creates hessian 'matrix' (vectors in vector)
	fi partials;
	fij hessian	;
	
	partials.push_back(dxxHimmelblau); partials.push_back(dxyHimmelblau);
	hessian.push_back(partials);

	partials.clear();

	partials.push_back(dyxHimmelblau); partials.push_back(dyyHimmelblau);
	hessian.push_back(partials);

	return hessian;
}

// Harmonic

double Harmonic(vec& xvec){
	return xvec[0]*xvec[0] + 2*xvec[1]*xvec[1];
}

double dxHarmonic(vec& xvec){
	return 2*xvec[0];
}

double dyHarmonic(vec& xvec){
	return 4*xvec[1];
}

fi HarmonicGrad(){
	fi grad;
	grad.push_back(dxHarmonic);
	grad.push_back(dyHarmonic);

	return grad;
}
