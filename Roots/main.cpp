#include <iostream>
#include <armadillo>
#include <cmath>
#include <assert.h>
#include "RootFinder.hpp"
#include "UsableFunctions.hpp"

using namespace std;
using namespace arma;
using fi = std::function<double(vec&)>; // single function fi takes the x vector and returns the evaluated value
using fs = std::vector<fi>; // vector that holds all functions fi

void RunProgram(RootFinder RF);

int main(){
	
	RootFinder RF;

	RF.QuadLinesearch = false;
	RunProgram(RF);	

	RF.QuadLinesearch = true;
	RunProgram(RF);	

	return 0;
}

void RunProgram(RootFinder RF){
	cout << "------------ ";
	RF.QuadLinesearch?  cout << "\e[1mRunning with Quadratic interp. linesearch\e[0m" : cout << "\e[1mRunning with simple backtrack linesearch\e[0m" ;
	cout << " ------------";

	double tolerance = 10e-9; //pow(10,-9); 
	double dx = pow(10,-10);


	// initial guess
	vec xInitial(2);
	//xInitial(0) = 2; xInitial(1) = 3;
	xInitial(0) = 6; xInitial(1) = 3;
	
	
	////////////////// func1 and func2	
	fs functions;	
	std::vector<fs> derivatives;
	functions.push_back(func1);// derivatives[0].push_back(dfunc1dx); derivatives[0].push_back(dfunc1dy); 
	functions.push_back(func2);
	cout << endl << "Roots for { A*x*y - 1 = 0,  exp(-x) + exp(-y) - 1 -1/A = 0 }: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);
	RF.SaveIterationPoints();	
	// Setup analytical 2x2 Jacobian
	RF.SetAnalyticJacobianStrategy();
	fs derivs1, derivs2; // function references	
	
	derivs1.push_back(dfunc1dx); derivs1.push_back(dfunc1dy); 
	derivs2.push_back(dfunc2dx); derivs2.push_back(dfunc2dy); 

	derivatives.push_back(derivs1); derivatives.push_back(derivs2);

	RF.derivatives = derivatives;

	cout << "Now using analytical Jacobian: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);

	////////////////// Rosenbrock gradient root finding
	RF.SetNumericJacobianStrategy();
	functions.clear();
	functions.push_back(dxRosenbrock);
	functions.push_back(dyRosenbrock);
	cout << endl << "Rosenbrock gradient roots, i.e. Rosenbrock extremum: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);

	// analytically:
	RF.SetAnalyticJacobianStrategy();
	derivatives.clear();derivs1.clear();derivs2.clear();
	derivs1.push_back(dxRosenbrockdx); derivs1.push_back(dxRosenbrockdy); 
	derivs2.push_back(dyRosenbrockdx); derivs2.push_back(dyRosenbrockdy); 

	derivatives.push_back(derivs1); derivatives.push_back(derivs2);

	RF.derivatives = derivatives;

	cout << "Now using analytical Jacobian: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);
	////////////////// Himmelblau gradient root finding
	RF.SetNumericJacobianStrategy();
	functions.clear();
	functions.push_back(dxHimmelblau);
	functions.push_back(dyHimmelblau);
	cout << endl << "Himmelblau gradient roots, i.e. Himmelblau extremum: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);


	////////////////// f(x,y) = 4*(x-3)^2 + 2*(y+2)^2 roots of gradient: 
	functions.clear();
	functions.push_back(dxHarmonicSum);
	functions.push_back(dyHarmonicSum);
	cout << endl << "f(x,y) = 4(x-3)^2 + 2(y+2)^2 minimum: "  << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);

	// analytical 2x2 Jacobian
	RF.SetAnalyticJacobianStrategy();
	derivatives.clear();derivs1.clear(); derivs2.clear(); // function references	
	
	derivs1.push_back(dxHarmonicSumdx); derivs1.push_back(dxHarmonicSumdy); 
	derivs2.push_back(dyHarmonicSumdx); derivs2.push_back(dyHarmonicSumdy); 

	derivatives.push_back(derivs1); derivatives.push_back(derivs2);


	RF.derivatives = derivatives;

	cout << "Now using analytical Jacobian: " << endl;
	RF.FindRoot(functions,xInitial,dx,tolerance);

}
