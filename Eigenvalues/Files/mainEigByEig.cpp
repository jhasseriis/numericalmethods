#include <iostream>
#include "Jacobi.hpp"
#include <chrono>

using namespace std;
using namespace arma;
using namespace std::chrono;

int main(){
    high_resolution_clock::time_point t1;
	high_resolution_clock::time_point t2;
	cout << "---------------" << endl;
	Jacobi Jac;

	mat A(3,3); A.zeros();
	double accuracy;	
	// consecutive on row
	cout << "using consecutive element method" << endl;
	int method = 0; 
	A <<
	1 << 2 << 4 << endr <<
	2 << 0 << 3 << endr <<
	4 << 3 << 6 << endr;	


	cout << " A = " << endl << A;
	accuracy = pow(10,-12);
	cout << endl;

	t1  = high_resolution_clock::now();
	Jac.EigbyEig(A,accuracy,method);
	t2 = high_resolution_clock::now();
	cout << "Eigenvalues of A:  " << trans(A.diag()) ;
	
    auto duration = duration_cast<microseconds>( t2 - t1 ).count();
    cout << "Execution of consecutive method in microseconds: " <<  duration << endl;


	cout << "---------------" << endl;
	cout << "using largest element method" << endl;
	// using largest 
	method = 1;

	A <<
	1 << 2 << 4 << endr <<
	2 << 0 << 3 << endr <<
	4 << 3 << 6 << endr;	


	cout << " A = " << endl << A;
	accuracy = pow(10,-12);
	cout << endl;
	t1  = high_resolution_clock::now();
	Jac.EigbyEig(A,accuracy,method);
    t2 = high_resolution_clock::now();
	cout << "Eigenvalues of A:  " << trans(A.diag()) ;

    duration = duration_cast<microseconds>( t2 - t1 ).count();
    cout << "Execution of largest method in microseconds: " <<  duration << endl;
	return 0;


}
