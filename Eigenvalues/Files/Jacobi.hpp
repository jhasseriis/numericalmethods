#include <iostream>
#include <armadillo>
#include <math.h>
#include <cmath>

using namespace std;
using namespace arma;

class Jacobi{

public:
	void CyclicSweep(mat& A, double accuracy); // diagonalizes A in place D->A by cyclic sweeps
	void EigbyEig(mat& A, double accuracy, int method=0); // method = 0: eliminate from left to right consecutively for each row. Method = 1: find largest value in row and elinate

	void Sweep(mat& A,int* rotationCounter = NULL);
	void SweepSingleRow(mat& A,int p,int* rotationCounter=NULL);
	void ApplyRot(mat& A, int p, int q);
	

	double GetMaxAbsOffDiag(mat& A);	
	double GetMaxAbsInRow(mat& A,int i,int* index);
};
