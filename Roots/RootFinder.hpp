# pragma once

#include <iostream>
#include <armadillo>
#include <cmath>
#include <assert.h>
#include "../LinearEquations/GramSchmidt/GS.hpp"
#include <fstream>

using namespace std;
using namespace arma;
using fi = std::function<double(vec&)>; // function reference
using fs = std::vector<fi>; // vector of functions

class RootFinder{

public:	
	void FindRoot(double epsilon); // wraps the function below
	void FindRoot(fs new_functions, vec xstart, double dx, double epsilon);
	void BacktrackLinesearch(); // finds lambda (needs FindNewtonStep called before-hand)
	//void BacktrackLinesearchQuad(); // linesearch with quad. interp.
	void FindNewtonStep(vec& xvec,double dx); // Calculates the step Deltax from matrix eq. J*Deltax=-f(x)
	void JacobianAnalyticEval(vec& xvec);
	void JacobianNumericEval(vec& xvec); 
	void JacobianNumericEval(mat& J, vec& xvec, double dx, fs& functions);
	void fEval(vec& x); // Evaluates the vector f(x)
	vec f(vec& x); // Evaluates value of f(x), but returns it as vector instead of writing it to variable

	void SaveIterationPoints(); // save the calculated x1,x2,...,xn for each iteration to file

	// Constructors
	RootFinder(){};	
	RootFinder(fs functions);
	RootFinder(fs functions, vec x,double dx);
		
	// Setup / reset
	void SetNewSystem(fs new_functions, vec new_x); // sets new system by calling the three functions below
	void SetFuncs(fs new_functions);
	void Setx(vec nex_x);
	void SetupJacobian(); // make new empty matrix with correct dimensions
	void SetNumericJacobianStrategy();
	void SetAnalyticJacobianStrategy();

	// vars
	GS Solver; // used to solve Jdx = - f(x)
	mat J;
	void (RootFinder::*JacobianEvalStrategy)(vec&) = &RootFinder::JacobianNumericEval; // pointer to either Numeric or Analytic derivative functions
//	void (RootFinder::*LinesearchStrategy)() = &RootFinder::BacktrackLinesearch; // pointer to either backtracklinesearch or quadratic backtrack interp. linesearch

	fs functions;
	std::vector<fs> derivatives; // there are n^2 derivatives. The i'th  element of this vector contains the partial derivatives for the i'th equation
	bool QuadLinesearch = true;
	vec x;
	vec fx;
	vec NewtonStep;
	double lambdaMinimum = 1.0/128.0;
	double dx;
	int countSteps = 0;
	int countLoopRuns = 0;
	std::vector<vec> iterationPoints;

};

