#include "LinearInterpolator.hpp"


LinearInterpolator::LinearInterpolator(vec xi,vec yi):
	xi(xi), yi(yi), pi(xi.n_elem)
{
	double dx,dy;
	for(int i=0; i < xi.n_elem-1; i++){
		dx=xi(i+1)-xi(i);
		dy=yi(i+1)-yi(i);
		pi(i) = dy/dx;
	}			
};

LinearInterpolator::~LinearInterpolator(){};

double LinearInterpolator::evaluate(double z){
	vec subArray = xi;
	vec tempArray;
	
	double midElemVal;  
	double midElemIndex;

	while(subArray.n_elem > 2){
		midElemIndex = floor(subArray.n_elem/2);
		midElemVal = subArray(midElemIndex);
			if(midElemVal > z){
				subArray = splitArray(subArray,0,midElemIndex);
			}
			else{
				subArray = splitArray(subArray,midElemIndex,subArray.n_elem-1);
			}
	}
	double lowerIntervalValue = subArray(0); // there are only 2 elements left
	int intervalIndex = findIndex(xi,lowerIntervalValue); // find the correct index

//	cout << "yi(intervalIndex) : " << yi(intervalIndex) << endl;
//	cout << xi(intervalIndex) << " < " << z << " < " << xi(intervalIndex+1) << endl;
	
	return yi(intervalIndex)+pi(intervalIndex)*(z - xi(intervalIndex));
};


// This function splits an array from lowerIndex to upperIndex and returns it in a new array
vec LinearInterpolator::splitArray(vec array,int lowerIndex,int upperIndex){
	int arrayLength = (upperIndex-lowerIndex)+1;
	vec returnArray(arrayLength);	
	int newIndex=0;

	for(int i=0;i<array.n_elem;i++){
		if(i>=lowerIndex && i<=upperIndex){
			returnArray(newIndex) = array(i);
			newIndex++;
		}
	}
	return returnArray;
}


void LinearInterpolator::exportToTxt(vec x){
	ofstream file;
	file.open(fileWriteName);
	cout << "Writing to file : " << fileWriteName << endl;		
//	file << endl << "# Tabulated points" << endl;

	for(int i=0; i<xi.n_elem;i++){
		file << xi(i) << "\t" << yi(i)<< endl;	
	}

//	file << endl << "# Interpolated points" << endl;
	file << endl << endl;;
	for(int i=0; i<x.n_elem;i++){
		file  << x(i) << "\t"  << evaluate(x(i)) << endl;
	}
	file.close();
}


int LinearInterpolator::findIndex(vec array,double value){

	for(int i=0; i<array.n_elem; i++){
		if(array(i) == value) return i;	
	}
	cout << "ERROR: No value " << value << " in this array! Returning -1";
	return -1; 
}



/*
 * Not implemented atm
void LinearInterpolator::pointWiseCheck(){
	int length = xi.n_elem;
	bool correct = true;
	for(int i=0; i<length;i++){
		if(yi(i) != evaluate(yi(i))){
		   	cout << "Si(xi) != yi(i) on index " << i << endl ;
			correct = false;
		}
	}
	
	if(correct) cout << "All points Si(xi) == yi(i) !!" << endl;
}
*/
