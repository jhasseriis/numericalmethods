#include "UsableFunctions.hpp"
#include "Minimizer.hpp"
#include "Minimizer.hpp"


using namespace arma;
using namespace std;
using f = std::function<double(vec&)>;
using fi = std::vector<f>;
using fij = std::vector<fi>;

int main(){

	vec xInitial(2); 
	xInitial(0) = 2 ; xInitial(1) = 3;

	cout << "------------------------------------------" << endl;
	cout << "Newton" << endl; 

	cout << "Rosenbrock: " ;
	Minimizer m(Rosenbrock,RosenbrockGrad(),RosenbrockHessian(),xInitial);
	m.Minimize();

	cout << "Himmelblau: ";
	m.SetSystem(Himmelblau,HimmelblauGrad(),HimmelblauHessian(),xInitial);
	m.Minimize();

	cout << "------------------------------------------" << endl;
	cout << "Broyden" << endl; 
	m.SetHessianInverseStrategyToBroyden();

	cout << "Rosenbrock: " ;
	m.SetSystem(Rosenbrock,RosenbrockGrad(),RosenbrockHessian(),xInitial);
	m.Minimize();

	cout << "Himmelblau: ";
	m.SetSystem(Himmelblau,HimmelblauGrad(),HimmelblauHessian(),xInitial);
	m.Minimize();

	cout << "Harmonic x*x+2*y*y: "; 
	m.SetSystem(Harmonic,HarmonicGrad(),HimmelblauHessian(),xInitial);
	m.Minimize();

	cout << endl << endl << "For some reason the divergence is strong for Himmelblau and Rosenbrock (using Broyden). Maybe it is because of the initial values, accuracy or a mix thereof. It works for a 2d harmonic function, so the algorithm is correct" << endl;

	return 0;
}

