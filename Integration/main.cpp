#include <iostream>
#include <armadillo>
#include "Integrator.hpp"
#include "../ODE/Driver.hpp"
#include "../ODE/Steppers.hpp"
#include <cmath>
#include <gsl/gsl_integration.h>

using namespace arma;
using namespace std;

using f = std::function<double(double)>;

double sqrtX(double x){ return sqrt(x);}
double sine(double x){ return sin(x);}
double xLin(double x){ return 2*x;}
double constant(double x) {return 1;}
double testFunction(double x) {return 4*sqrt(1-pow(1-x,2));}
double testFunctionGSL(double x,void* params){return 4*sqrt(1-pow(1-x,2));}
double gaussian(double x) { return exp(-x*x);}
//double divergence(double x){ return 1/(1-x*x);}
double divergence(double x){ return 1/sqrt(1-x*x);} 

// ode system
void odesystem(double t, vec& y, vec& dydt){
	dydt(0) = sine(t);
}

Integrator I;

double a = 0;
double b = 1;
double acc = 1e-4;
double epsi = 1e-4;

void Evaluate(f function,int mode=3){

	if(mode==1 || mode==3)
		cout << "Open:\t";
		cout << "Q ≈ " << I.Adapt_Open(function,a,b,acc,epsi);
		cout << "\t Recursion count: " << I.n_calls << endl;
	if(mode==2 || mode==3){
		cout << "Closed:\t" ;
		cout << "Q ≈ " << I.Adapt_Closed(function,a,b,acc,epsi);
		cout << "\t Recursion count: " << I.n_calls << endl;
	}
	cout << endl;

}


int main(){
	cout << "---------------------------------------------\n";
	cout.precision(15);
	// sine 
	cout << "sin(x) [0,1]:" << endl;
	Evaluate(sine);

	// sqrt
	cout << "sqrt(x) [0,1]:" << endl;
	Evaluate(sqrtX);

	// test function 
	cout << "My integrator: 4*sqrt(1-(1-x)^2) [0,1]: " << endl;
	Evaluate(testFunction);	

	// gsl integrator 	
	double result,err;
	gsl_integration_workspace * w  = gsl_integration_workspace_alloc (1000);
	gsl_function F;
	F.function = &testFunctionGSL;
	gsl_integration_qags(&F,a,b,acc,epsi,1000,w,&result,&err);
	gsl_integration_workspace_free (w);
	cout << "gsl_integration_qags: 4*sqrt(1-(1-x)^2) [0,1]: " << endl;
	cout << "Q ≈ " << result << "\terr: " << err << endl << endl;

	cout << "---------------------------------------------\n";

	// gaussian, only use open limits: the transformations are NaN for the closed case limits
	cout << "gaussian exp(-x^2)" << endl;
	cout << "should be sqrt(π)≈" << sqrt(M_PI)  << " from -inf to inf" << endl;
	a = -INFINITY;
	b =  INFINITY;
	cout << "[-inf,inf]: " << endl;
	Evaluate(gaussian,1);	

	cout << "[0, inf]: " << endl;
	a = 0;
	Evaluate(gaussian,1);
		
	cout << "[-inf, 0]: " << endl;
	a = -INFINITY;
	b = 0;
	Evaluate(gaussian,1);
	
	cout << "---------------------------------------------\n";
	
	// Clenshaw curtis	
	a = -1; b = 1;
	cout << "ClenshawCurtis on [-1,1]: 1/sqrt(1-x^2))" << endl;
	cout << "Q ≈ " << I.ClenshawCurtis(divergence,a,b,acc,epsi) << endl;

	// ODE solver for integration
	cout << endl << "ODE solver for sin(x) [-pi,pi]: " << endl;
	double h = 0.01, acc = 1e-6, eps = 1e-6;
	vec y(1); 
	y(0) = 0;
	Driver d(-M_PI,M_PI,h,acc,eps,y,odesystem,rkstep12);
	d.Solve();
	cout << "Q = y(b) ≈	" << d.y(0) << endl;

	// Integration
	a = -M_PI; b = M_PI;
	cout << endl << "Integration for sin(x) [-pi,pi]: " << endl;
	Evaluate(sine,1);

	return 0;
}



