#pragma once

#include <armadillo>
#include <math.h>
#include <fstream>

using namespace arma;
using namespace std;

class LinearInterpolator{
public:
	
	// Variables
	vec xi;
	vec yi;
	vec pi;
	string fileWriteName = "LinearInterpolator.txt";
	
	// Constructors and destructors
	LinearInterpolator(vec xi,vec yi);
	~LinearInterpolator();

	// Member functions
	double evaluate(double z);
	vec splitArray(vec array,int lowerIndex,int upperIndex);
	int findIndex(vec array,double value);
	void exportToTxt(vec x); // for arbitrary x vector, produce text output
};
