#include "Jacobi.hpp"

void Jacobi::CyclicSweep(mat& A, double accuracy){
	int n = A.n_cols;
	int count = 0;

	double maxAbs = GetMaxAbsOffDiag(A) ;

	int rotationCounter = 0;
		
	while( maxAbs > accuracy){
		Sweep(A,&rotationCounter);
		maxAbs = GetMaxAbsOffDiag(A);
		count++;
	//	cout << maxAbsOffDiag << endl;
	}

//	cout << endl << "Accuracy of " <<  accuracy << " obtained after " << count << " sweeps! " << endl;
	cout << endl << "Accuracy of " <<  accuracy << " obtained after " << rotationCounter << " rotations! " << endl;
}

void Jacobi::EigbyEig(mat& A, double accuracy, int method){
	int cols = A.n_cols;
	int rows = A.n_rows;
	int count = 0;

	double maxAbs;
	
	int index; 	int* indexPtr = NULL;
	indexPtr = &index;

	int rotationCounter=0;

	for(int i=0; i < rows; i++){
		maxAbs = GetMaxAbsInRow(A,i,indexPtr);
		while(maxAbs > accuracy){
			if(method == 0){
				// Consecutive method
				SweepSingleRow(A,i,&rotationCounter);	
				maxAbs = GetMaxAbsInRow(A,i,indexPtr);
			}
			else{ // if if finding largest element 
				ApplyRot(A,i,index);	
				rotationCounter++;
				maxAbs = GetMaxAbsInRow(A,i,indexPtr);
			}

			count++;
		}
	 //cout << "Row i: " << i << " - Accuracy of " <<  accuracy << " obtained after " << count << " row sweeps! " << endl;
		count =0;
	}

	 cout << "Accuracy of " <<  accuracy << " obtained after " << rotationCounter << " rotations! " << endl;

}

void Jacobi::SweepSingleRow(mat& A,int p,int* rotationCounter){
	int n = A.n_cols;

	for(int j=0; j < n;j++){
		if(p != j)	{
			ApplyRot(A,p,j);
			if(rotationCounter != NULL){
				*rotationCounter = *rotationCounter +1;
			}
		}
	}
};



void Jacobi::Sweep(mat& A,int* rotationCounter){
	int n = A.n_cols;

	for(int i=0; i < n; i++){
		for(int j=0; j < n; j++){
			if(i != j){
				ApplyRot(A,i,j);
				if(rotationCounter != NULL){
					*rotationCounter = *rotationCounter + 1;	
				}
			}	
		}
	}
}




// This implementation is not the most effective since I use a temporary local matrix A0 = A, and I transform both sides of the diagonal 
void Jacobi::ApplyRot(mat& A, int p, int q){
	
	mat A0 = A;

	int n = A.n_cols;

	double phi = 0.5*atan2(2.0*A(p,q),A(q,q)-A(p,p));
	double s = sin(phi);
	double c = cos(phi);

	double valp, valq, val0;

	// Offdiagonal elements
	for(int i=0; i < n; i++){		
		if(i != p && i != q){
			valp = c*A0(p,i)-s*A0(q,i);
			valq = s*A0(p,i)+c*A0(q,i);

			A(p,i) = valp;
			A(i,p) = valp;

			A(q,i) = valq;
			A(i,q) = valq;
		}
	}

	// Diagonal elements
	A(p,p) = c*c*A0(p,p) - 2.0*s*c*A0(p,q) + s*s*A0(q,q);
	A(q,q) = s*s*A0(p,p) + 2.0*s*c*A0(p,q) + c*c*A0(q,q);
	
	// Elements to be zeroed
//	val0 = s*c*(A0(p,p)-A0(q,q))+(c*c-s*s)*A0(p,q); 
//	A(p,q) = val0;
//	A(q,p) = val0;
	A(q,p) = 0;
	A(p,q) = 0;
};

double Jacobi::GetMaxAbsOffDiag(mat& A){
	int cols = A.n_cols;
	int rows = A.n_rows;
	
	double temporaryLargest = 0;
	double temporaryValue;	
	for(int i=0; i < rows; i++){
		for(int j=0; j < cols; j++){
			if(i != j){
				temporaryValue = abs(A(i,j));
					if(temporaryValue > temporaryLargest){
						temporaryLargest = temporaryValue;
					}
			} 
		}	
	}	
	return temporaryLargest;
}




double Jacobi::GetMaxAbsInRow(mat& A, int i, int* index){
	int cols = A.n_cols;
	
	double temporaryLargest = 0;
	double temporaryValue;
	for(int j=0; j < cols; j++){
		if(i != j){
			temporaryValue = abs(A(i,j));
				if(temporaryValue > temporaryLargest){
					temporaryLargest = temporaryValue;
					if(index != NULL){
						*index = j;
					}
				}
		} 
	}	
	return temporaryLargest;
}

