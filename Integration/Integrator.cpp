#include "Integrator.hpp"

Integrator::Integrator(){
}



///////////////// Closed  ///////////////////

double Integrator::Adapt_Closed(f initialIntegrand, double a, double b, double acc, double eps){
	n_calls = 0;


	int ainf = isinf(a); int binf = isinf(b);

	f integrand = initialIntegrand;
	// check if inf in any limit
	if(ainf==1 && binf==1){
	    integrand = [&](double x){ return initialIntegrand(x/(1-x*x))*(1+x*x)/pow(1-x*x,2);  };
//	    integrand = [&](double x){ return (initialIntegrand((1-x)/x)+initialIntegrand(-(1-x)/x))/(x*x);};
		a = -1;
		b = +1;
	}
	else if(ainf==1){
		integrand = [&](double x){ return initialIntegrand(a-(1-x)/x)/(x*x);};
		a = 0;
		b = 1;
	}
	else if(binf==1){
		integrand = [&](double x){ return initialIntegrand(a+(1-x)/x)/(x*x);};
		a = 0;
		b = 1;
	}

	//cout << "integrand(-1) " << integrand(-1) << endl; 	
	double f0 = integrand(a);
	double f2 = integrand(b);
	//cout << "f0: " << f0 << endl;
	//cout << "f2: " << f2 << endl;
	return Adapt12_Closed(integrand,a,b,acc,eps,f0,f2);
}


double Integrator::Adapt12_Closed(f integrand, double a, double b, double acc, double eps, double f0, double f2){

	
	n_calls++;
	double f1 = integrand(a+(b-a)/2.0);
	double Q = (f0+4*f1+f2)/6.0*(b-a);
	double q = (f0+2*f1+f2)/4.0*(b-a);

	double error = abs(Q-q);

	double tolerance = acc + eps*abs(Q);

	if(error < tolerance) return Q;
	else{
		double Q1 = Adapt12_Closed(integrand,a,(a+b)/2.0,acc/sqrt(2.0),eps,f0,f1);
		double Q2 = Adapt12_Closed(integrand,(a+b)/2.0,b,acc/sqrt(2.0),eps,f1,f2);
		return Q1+Q2;
	}
	
}

///////////////// Open ///////////////////

double Integrator::Adapt_Open(f initialIntegrand, double a, double b, double acc, double eps){
	n_calls = 0;

	int ainf = isinf(a); int binf = isinf(b);

	f integrand = initialIntegrand;
	// check if inf in any limit
	if(ainf==1 && binf==1){
		a = -1;
		b = +1;
	    integrand = [&](double x){ return initialIntegrand(x/(1-x*x))*(1+x*x)/pow(1-x*x,2);  };
	}
	else if(ainf==1){
		integrand = [&](double x){ return initialIntegrand(a-(1-x)/x)/(x*x);};
		a = 0;
		b = 1;
	}
	else if(binf==1){
		integrand = [&](double x){ return initialIntegrand(a+(1-x)/x)/(x*x);};
		a = 0;
		b = 1;
	}

	double f2 = integrand(a+2*(b-a)/6.0);
	double f3 = integrand(a+4*(b-a)/6.0);
	return Adapt24_Open(integrand,a,b,acc,eps,f2,f3);
}

double Integrator::Adapt24_Open(f integrand, double a, double b, double acc, double eps, double f2, double f3){

	n_calls++;

	double f1 = integrand(a+(b-a)/6.0);
	double f4 = integrand(a+5*(b-a)/6.0);

	double Q = (2*f1+f2+f3+2*f4)/6.0*(b-a); // trapz
	double q = (f1+f2+f3+f4)/4.0*(b-a); // rect

	double tolerance = acc + eps*abs(Q);
	double error = abs(Q-q);

	if(error < tolerance) return Q;
	else{
		double Q1 = Adapt24_Open(integrand,a,(a+b)/2.0,acc/sqrt(2.0),eps,f1,f2);
		double Q2 = Adapt24_Open(integrand,(a+b)/2.0,b,acc/sqrt(2.0),eps,f3,f4);
		return Q1+Q2;
	}
}

/////////// Clenshaw Curtis transform //////////

double Integrator::ClenshawCurtis(f initialIntegrand,double a,double b, double acc, double eps){
	f integrand = [&](double theta){
//		( ( a + b ) + ( b - a ) * x[i] ) / 2.0;
		return initialIntegrand(cos(theta))*sin(theta);
	};
	a = 0;
	b = M_PI;
	
	return Adapt_Open(integrand,a,b,acc,eps);
}

/////////// 2-d open ////////////////

double Integrator::Adapt_Open_2dim(std::function<double(vec)> integrand,vec a,vec b,double acc, double eps){

	f Fxy = [&](double x){
		f Fy = [&](double y){
			vec coordinates(2);
			coordinates(0) = x;
			coordinates(1) = y;
			return integrand(coordinates);
		};
		double Qy = Adapt_Open(Fy,a(1),b(1),acc,eps);
		return Qy;
	};
	
	double Qxy = Adapt_Open(Fxy,a(0),b(0),acc,eps);
	return Qxy;
}
