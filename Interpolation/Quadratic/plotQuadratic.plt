set term aqua
set key outside 
set title "Quadratic interpolation"
#set xrange [0:3]
#set yrange [0:2]

plot "QuadraticInterpolator.txt" index 0 title 'Tabulated points',\
"QuadraticInterpolator.txt" index 1 with lines title 'Interpolated points', "QuadraticInterpolator.txt" index 1 using 1:3 with lines title 'Derivative', "QuadraticInterpolator.txt" index 1 using 1:($4-1) with lines title 'Integral', -cos(x) title '-cos(x)' 
#, x**3/3 title 'x^3/3'
