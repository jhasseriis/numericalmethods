#include "BilinearInterpolator.hpp"


void BilinearInterpolator::GenerateGrid(vec& xlim, int nx,vec& ylim,int ny){

	xi = linspace<vec>(xlim(0),xlim(1),nx);
	yi = linspace<vec>(ylim(0),ylim(1),ny);

	this->nx = nx;
	this->ny = ny;

}

void BilinearInterpolator::SetGrid(vec xi, vec yi){
	
	this->xi = xi;
	this->yi = yi;

	this->nx = xi.n_elem;
	this->ny = yi.n_elem;
}	


// 

void BilinearInterpolator::InterpolateGrid(fxy Fxy, vec& px, vec& py){
	

	int	nx_interp = px.n_elem;
	int	ny_interp = py.n_elem;

	this->px = px;
	this->py = py;

	F = zeros<mat>(nx_interp,ny_interp);
	for(int i=0; i < nx_interp; i++){
		for(int j=0; j < ny_interp; j++){
			//cout << "i: " << i << ", j: " << j << endl;
			F(i,j) = (*this.*InterpolatePointStrategy) (Fxy,px(i),py(j));
//			F(i,j) = InterpolatePoint(Fxy,px(i),py(j));
		}
	}
}

// This method uses my implementation of the GramSchmidt procedure to solve the linear system of equations
double BilinearInterpolator::InterpolatePointWithLinearSystem(fxy Fxy, double x, double y){

	assert(x >= xi(0));	assert(x <= xi(nx-1));
    assert(y >= yi(0));	assert(y <= yi(ny-1));
	//cout << "x: " << x << ", y: " << y << endl;
	FindNearestPoints(x,y);
	double x1 = nearestpoints.x1y1.at(0); double x2 = nearestpoints.x2y1.at(0);
	double y1 = nearestpoints.x1y1.at(1); double y2 = nearestpoints.x2y2.at(1);

	System << 1 << x1 << y1 << x1*y1 << endr
		   << 1 << x1 << y2 << x1*y2 << endr
		   << 1 << x2 << y1 << x2*y1 << endr
		   << 1 << x2 << y2 << x2*y2 << endr;

	RHS << Fxy(x1,y1) << Fxy(x1,y2) << Fxy(x2,y1) << Fxy(x2,y2);

	decomposer.decomp(System,R);
	decomposer.solve(System,R,RHS,coefficients); // solves QRx=RHS for the coefficient vector 'x'

	return coefficients(0) + coefficients(1)*x + coefficients(2)*y + coefficients(3)*x*y;
}


double BilinearInterpolator::InterpolatePointSimple(fxy Fxy,double x, double y){

	assert(x >= xi(0));	assert(x <= xi(nx-1));
    assert(y >= yi(0));	assert(y <= yi(ny-1));
	//cout << "x: " << x << ", y: " << y << endl;
	FindNearestPoints(x,y);
	double x1 = nearestpoints.x1y1.at(0); double x2 = nearestpoints.x2y1.at(0);
	double y1 = nearestpoints.x1y1.at(1); double y2 = nearestpoints.x2y2.at(1);

	return 1/((x2-x1)*(y2-y1))*(
			Fxy(x1,y1)*(x2-x)*(y2-y) +
			Fxy(x2,y1)*(x-x1)*(y2-y) +
			Fxy(x1,y2)*(x2-x)*(y-y1) +
			Fxy(x2,y2)*(x-x1)*(y-y1)
			)		 
		 ; // this algorithm is from the wikipage for Bilinear interpolation
	
}

void BilinearInterpolator::UseLinearSystemStrategy(){
	cout << "Switching to 'linear system' strategy..." << endl;
	InterpolatePointStrategy = &BilinearInterpolator::InterpolatePointWithLinearSystem;
}

void BilinearInterpolator::UseSimpleStrategy(){
	cout << "Switching to 'simple' strategy..." << endl;
	InterpolatePointStrategy = &BilinearInterpolator::InterpolatePointSimple;
}

void BilinearInterpolator::FindNearestPoints(double x,double y){

	vec auxVec(2); 

	auxVec(0) =  x;
	auxVec(1) =  y;

	nearestpoints.coordinates = auxVec;


	int i = 0, j = 0;
	while(x > xi(i)){
		i++;	
	}
	while(y > yi(j)){
		j++;		
	}
	//cout << "x: " << x << ", y: " << y << endl;

	// This check is needed (due to the way I implemented the while loops)
	if(x == xi(0)) i = 1;
	if(y == yi(0)) j = 1; 

	// x1y1
	auxVec(0) = xi(i-1); auxVec(1) = yi(j-1);
	nearestpoints.x1y1 = auxVec; 

	// x1y2
	auxVec(1) = yi(j);
	nearestpoints.x1y2 = auxVec;

	// x2y2
	auxVec(0) = xi(i);
	nearestpoints.x2y2 = auxVec;

	// x2y1
	auxVec(1) = yi(j-1);
	nearestpoints.x2y1 = auxVec;

}

//////////////// print and write
 
void BilinearInterpolator::PrintNearestPoints(){
	cout <<	 "(" << nearestpoints.x1y1[0] << "," << nearestpoints.x1y1[1] << ")\t"; 
	cout <<	 "(" << nearestpoints.x1y2[0] << "," << nearestpoints.x1y2[1] << ")\n"; 
	cout <<	 "(" << nearestpoints.x2y1[0] << "," << nearestpoints.x2y1[1] << ")\t"; 
	cout <<	 "(" << nearestpoints.x2y2[0] << "," << nearestpoints.x2y2[1] << ")\n"; 
}

void BilinearInterpolator::PrintGrid(){
	cout << "(x,y) =" << endl;
	for(int i=0; i<nx;i++){
		for(int j=0; j < ny; j++){
			cout << "(" << xi(i) << "," << yi(j) << ")   "; 
		}
		cout << endl;
	}
}

void BilinearInterpolator::WriteGridToFile(){
	
	ofstream myfile;
	myfile.open("output/Grid.txt");

	for(int i=0; i<nx;i++){
		for(int j=0; j < ny; j++){
			myfile << xi(i) << "\t" << yi(j) << "\t" << "0" << endl; // how gnuplot wants z(x,y) 
		}
		myfile << endl;
	}

	myfile.close();
}

void BilinearInterpolator::WriteNearestNeighborToFile(){

	ofstream myfile;
	myfile.open("output/NearestNeighbor.txt");

	myfile << nearestpoints.coordinates[0] << "\t" << nearestpoints.coordinates[1] << "\t0" << endl << endl;

	myfile << nearestpoints.x1y1[0] << "\t" << nearestpoints.x1y1[1] << "\t0" << endl;
	myfile << nearestpoints.x1y2[0] << "\t" << nearestpoints.x1y2[1] << "\t0" << endl;
	myfile << endl;
	myfile << nearestpoints.x2y1[0] << "\t" << nearestpoints.x2y1[1] << "\t0" << endl;
	myfile << nearestpoints.x2y2[0] << "\t" << nearestpoints.x2y2[1] << "\t0" << endl;

	myfile.close();
}


void BilinearInterpolator::WriteInterpolationToFile(){
	ofstream myfile;
	myfile.open("output/Interpolation.txt");

	for(int i = 0; i<F.n_rows; i++){
		for(int j=0; j<F.n_cols; j++){
			myfile << px(i) << "\t" << py(j) << "\t" << F(i,j) << endl;
		}
		myfile << endl;
	}
	myfile.close();

	myfile.open("output/InterpolationGrid.txt");
	for(int i = 0; i < nx; i++){
		for(int j = 0; j < ny; j++){
			myfile << xi(i) << "\t" << yi(j) << "\t 1" << endl;
		}
		myfile << endl;
	}

	myfile.close();

}
