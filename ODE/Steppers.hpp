#pragma once
#include <iostream>
#include <armadillo>

using namespace arma;

// t: current point, h: step size, y: current values, f: function that calculates dydt from current point and values, yh: the next values in point y+h, err: error of the step
void rkstep12(double t, double h, vec& y, void f(double t, vec& y, vec& dydt),vec& yh, vec& err,int* n_evals);




void rkstep2_non_embedded(double t, double h, vec& y, void f(double t, vec& y, vec& dydt),vec& yh, vec& err,int* n_evals);
