#include <iostream>
#include <armadillo>
#include "MonteCarlo.hpp"
#include "../Integration/Integrator.hpp"

using namespace std;
using namespace arma;

using func = std::function<double(vec)>;

double sinxcosy(vec coordinates){
	double x = coordinates(0), y = coordinates(1);
	return sin(x)*cos(y);
}

double harmonic(vec coordinates){
	double x = coordinates(0), y = coordinates(1);
	return x*x+y*y;
}

double difficult(vec coordinates){	
	double x = coordinates(0), y = coordinates(1), z = coordinates(2);
	return 1/pow(M_PI,3)*1/(1-cos(x)*cos(y)*cos(z));
}

MonteCarlo MC;
Integrator I;

void EvaluateWithText(func function,vec& a,vec& b,int N,double* err){
	cout << "N: " << N << ", ";
	cout << "Box boundaries: ";

	cout << "a = [  ";
	for(int i=0; i<a.n_elem;i++) cout << a(i) << "  " ;
	cout << "]\t"; 

	cout << "b = [  ";
	for(int i=0; i<b.n_elem;i++) cout << b(i) << "  " ;
	cout << "]" << endl;

	cout << "Q ≈ " <<	MC.PlainIntegrate(function,a,b,N,err) << endl;	
	if(err!=NULL) cout << "error ≈ " << *err << endl;
}


int main(){
	
	vec a(2);vec b(2);
	a(0) = 0; a(1) = 0;
	b(0) = 1; b(1) = 1;
	int N = 1e6;
	double err;

	cout << "-----------------------------------------------" << endl;
	// sin(x)*cos(y)
	cout << endl <<  "sinx*cosy: " << endl;
	EvaluateWithText(sinxcosy,a,b,N,&err);

	// harmonic2d
	cout << endl << "x*x+y*y: " << endl;
	EvaluateWithText(harmonic,a,b,N,&err);	


	// check O(1/sqrt(N)) precision of err
	cout << endl << "Checking if the error precision scales as O(1/sqrt(N)) by incrementing N by 10 and comparing the errors: O(1/sqrt(N)/O(1/sqrt(10*N)) ~ sqrt(10) ≈ 3.1623" << endl;
	double err_prev;	
	N=10;
	MC.PlainIntegrate(sinxcosy,a,b,N,&err);
//	cout << "N: " << N << "\t err: " << err<< endl; 	
	for(N = 100; N < 1e7; N*=10){
		err_prev=err;
		MC.PlainIntegrate(sinxcosy,a,b,N,&err);
		cout << "O(1/sqrt(" << N/10 << ")) / O(1/sqrt(" << N  << ")) ≈ "<< err_prev/err << endl; 	
	}

	
	// checking difficult function
	cout << endl << "Integrating 1/π^3*(1-cos(x)cos(y)cos(z))^(-1): expecting ≈ 1.3932039... " << endl;
	a = zeros<vec>(3); b = zeros<vec>(3);
	b(0) = M_PI; b(1) = M_PI; b(2) = M_PI;
	N = 100000;
	EvaluateWithText(difficult,a,b,N,&err);

	cout << "-----------------------------------------------" << endl;

	a = zeros<vec>(2); b = zeros<vec>(2);
	b(0) = 1; b(1) = 1	;
		
	// With tolerance
	N = 10;
	double tol = 1e-3;

	cout << "Monte Carlo integrator with tolerance: " << tol << endl;
	cout << "sinxcosy:\tQ ≈ " <<  MC.PlainIntegrateWithTolerance(sinxcosy,a,b,tol,&err) <<  "   (N: " << MC.N << ", err: " << MC.err<< ")" <<endl;
	cout << "x*x+y*y: \tQ ≈ " <<  MC.PlainIntegrateWithTolerance(harmonic,a,b,tol,&err) << "   (N: " << MC.N << ", err: " << MC.err<< ")" <<endl;



	// adaptive integrator 2d
	double	acc = 1e-3; double eps = 1e-3;
	cout << endl << "2d adaptive integrator with tolerance: " << acc << endl;
	cout << "sinxcosy: Q ≈ " << I.Adapt_Open_2dim(sinxcosy,a,b,acc,eps) << endl;
	cout << "x*x+y*y: Q ≈ " << I.Adapt_Open_2dim(harmonic,a,b,acc,eps) << endl;


	return 0;

}
