#include "QuadraticInterpolator.hpp"

// The constructor builds pi and ci; then you can find  any S(x) between the boundary  x values !
QuadraticInterpolator::QuadraticInterpolator(vec xi,vec yi):
	xi(xi), yi(yi), pi(xi.n_elem-1), ci(xi.n_elem-1),SiIntegrals(xi.n_elem-1)
{
	// There are n-1 'line' segments
	double dx,dy;
	vec dxi(xi.n_elem-1);	// Calculate pi(i)
	for(int i=0; i < xi.n_elem-1; i++){
		dx=xi(i+1)-xi(i);
		dy=yi(i+1)-yi(i);
		pi(i) = dy/dx;
		dxi(i) = dx;
	}			

	// Recursive calculation of ci(i)	
	// calc ci+1 from ci etc., we take c1=0 at first
	cout << endl << "Performing forward recursion...";
	for(int i=0; i<ci.n_elem-1;i++){ // note -1, 
		ci(i+1) = 1/dxi(i+1)*(pi(i+1)-pi(i)-ci(i)*dxi(i));
		//cout << "i: " << i << endl;
	}
	
	cout << " DONE!" << endl;
	// Now calc backwards beginning with a start value of the found result frem the previous recursion
	
	ci(ci.n_elem-1) = 0.5*ci(ci.n_elem-1);	
	cout <<  "Performing backward recursion...";
	for(int i=ci.n_elem-2; i>=0 ;i--){ // note -2
		ci(i) = 1/dxi(i)*(pi(i+1)-pi(i)-ci(i+1)*dxi(i+1));
		//cout << "i: " << i << endl;
	}
	cout << " DONE!" << endl << endl;

	bi = pi - ci % dxi; // % is elementwise multiplication	

	double lower, upper;
	// Calculation of integration over each Si segment
	for(int i=0;i<SiIntegrals.n_elem;i++){
	
		lower =	
		 yi(i)*xi(i) -
		 bi(i)*pow(xi(i),2)/2.0 +
		 ci(i)*pow(xi(i),3)/3.0;

		upper =
			 yi(i)*xi(i+1) +
			 bi(i)*(pow(xi(i+1),2)/2.0-xi(i)*xi(i+1))+
			 ci(i)*(pow(xi(i+1),3)/3.0-xi(i)*pow(xi(i+1),2)+xi(i)*xi(i)*xi(i+1));

		
		SiIntegrals(i) = upper - lower;
	 }
};

QuadraticInterpolator::~QuadraticInterpolator(){};

double QuadraticInterpolator::derivative(double z){
	return evaluate(z,"Derivative");
}

double QuadraticInterpolator::integrate(double z){
	return evaluate(z,"Integrate");
}

double QuadraticInterpolator::evaluate(double z,string type=""){	
	vec subArray = xi;
	vec tempArray;
	
	double midElemVal;  
	double midElemIndex;

	while(subArray.n_elem > 2){
		midElemIndex = floor(subArray.n_elem/2);
		midElemVal = subArray(midElemIndex);
			if(midElemVal > z){
				subArray = splitArray(subArray,0,midElemIndex);
			}
			else{
				subArray = splitArray(subArray,midElemIndex,subArray.n_elem-1);
			}
	}
	double lowerIntervalValue = subArray(0); // there are only 2 elements left
	int index = findIndex(xi,lowerIntervalValue); // find the correct index

	//cout << "yi(index) : " << yi(index) << endl;
	//cout << xi(index) << " < " << z << " < " << xi(index+1) << endl;
	
//	double S = yi(index)+
//		   pi(index)*(z - xi(index))+
//		   ci(index)*( z -xi(index))*(z - xi(index+1));
	double result=0;
	if(type==""){ // if simply evaluating Si	
		result =  yi(index)+
				bi(index)*(z-xi(index))+
				ci(index)*(z-xi(index))*(z-xi(index));
	}
	if(type=="Derivative"){
		result = bi(index)+2*ci(index)*(z-xi(index));	
	}
	if(type=="Integrate"){
		// First sum contributions from x1 to xi(index)-1 
		for(int i=0; i<index-1;i++){	
			result = result + SiIntegrals(i);
		}
		// integrate from xi(index) to z	
	   double lower =	
		 yi(index)*xi(index) -
		 bi(index)*pow(xi(index),2)/2.0 +
		 ci(index)*pow(xi(index),3)/3.0;

		double upper =  
		 yi(index)*z +
		 bi(index)*(pow(z,2)/2.0-xi(index)*z)+
		 ci(index)*(pow(z,3)/3.0-xi(index)*pow(z,2)+pow(xi(index),2)*z);

		result = result + upper - lower;
	}

	return result;
};


// This function splits an array from lowerIndex to upperIndex and returns it in a new array
vec QuadraticInterpolator::splitArray(vec array,int lowerIndex,int upperIndex){
	int arrayLength = (upperIndex-lowerIndex)+1;
	vec returnArray(arrayLength);	
	int newIndex=0;

	for(int i=0;i<array.n_elem;i++){
		if(i>=lowerIndex && i<=upperIndex){
			returnArray(newIndex) = array(i);
			newIndex++;
		}
	}
	return returnArray;
}


void QuadraticInterpolator::exportToTxt(vec x){
	ofstream file;
	file.open(fileWriteName);
	cout << "Writing to file : " << fileWriteName << endl;		
//	file << endl << "# Tabulated points" << endl;

	for(int i=0; i<xi.n_elem;i++){
		file << xi(i) << "\t" << yi(i)<< endl;	
	}

//	file << endl << "# Interpolated points" << endl;
	file << endl << endl;;
	for(int i=0; i<x.n_elem;i++){
		file  << x(i) << "\t"  << 
			evaluate(x(i)) << "\t" << 
			derivative(x(i)) << "\t" << 
			integrate(x(i)) <<
			endl;
	}
	file.close();
}


int QuadraticInterpolator::findIndex(vec array,double value){

	for(int i=0; i<array.n_elem; i++){
		if(array(i) == value) return i;	
	}
	cout << "ERROR: No value " << value << " in this array! Returning -1";
	return -1; 
}



