# Grid plot
# set terminal aqua 
set terminal pdf
set output 'output/GridPlot.pdf'
set xlabel 'x'
set ylabel 'y'
set zrange[0:5]
set view 0,0,1,1

splot 'output/Grid.txt' title 'Rectilinear grid lines' with lines, 'output/NearestNeighbor.txt' title 'Nearest neighbor test for randomly generated point'
