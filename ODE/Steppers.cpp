#include "Steppers.hpp"

void rkstep12(double t, double h, vec& y, void f(double t, vec& y,vec& dydt),vec& yh,vec& err,int* n_evals){	

	int n = y.n_elem;
	vec k0(n); vec k12(n); vec k(n);

	f(t,y,k0); // k0 = f(t,y), save dydt result in k0
	vec auxVec = y + 0.5*h*k0; // auxillary vector due to armadillo bindings
	f(t+0.5*h , auxVec , k12); // k12 = k = f(t+1/2*h, y+1/2*h*k0), save in k1/2
	k = k12; // not necessary, but whatever

	yh = y + h*k; // next values
	err = (k0-k12)*h/2; // error estimate (revise?)

	*n_evals = *n_evals + 1;
	//cout << *n_evals << endl;
}


void rkstep2_non_embedded(double t, double h, vec& y, void f(double t,vec& y, vec& dydt),vec& yh, vec& err,int* n_evals){
	int order = 2;

	vec y_half = zeros<vec>(y.n_elem);
	vec y_full = y_half;

	// fullstep: store result in yh, leaving y unchanged
	rkstep12(t,h,y,f,y_full,err,n_evals);
	
//	cout << "t: " << t << endl;
	// 2 half steps: store resul in y_half
	rkstep12(t,h/2,y,f,y_half,err,n_evals);
	rkstep12(t+h/2,h/2,y_half,f,yh,err,n_evals);

	err = (y_full-yh)/(pow(2,order)-1);

}
