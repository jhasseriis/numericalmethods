#pragma once 

#include <iostream>
#include <armadillo>
#include <fstream>
#include <assert.h>
#include "../../LinearEquations/GramSchmidt/GS.hpp"

using namespace std;
using namespace arma;

using fxy = std::function<double(double,double)>;

struct NearestPoints{
	vec coordinates; // the coordinate values with following nearest neighbors:
	vec x1y1,x1y2;  // Q11, Q12
	vec x2y1,x2y2;  // Q21, Q22
};

class BilinearInterpolator{
public:

	BilinearInterpolator(){};

	void InterpolateGrid(fxy Fxy,vec& x, vec& y);
	double InterpolatePointSimple(fxy Fxy,double x,double y);
	double InterpolatePointWithLinearSystem(fxy Fxy,double x,double y);

	void UseLinearSystemStrategy(); // set strategy to interpolate point to linear system
	void UseSimpleStrategy(); // set strategy to interpolate point to simple algorithm

	void FindNearestPoints(double x, double y);  // given coordinates (x,y) find the rectangle it belongs to, ie. the enclosing points {(x1,y1),(x1,y2),(x2,y1),(x2,y2)} and save in struct

	void GenerateGrid(vec& xlim,int nx,vec& ylim,int ny); // generate regular grid 
	void SetGrid(vec xi,vec yi); // set the grid to given vectors 
	void PrintNearestPoints();
	void PrintGrid(); // print (x,y) coordinates in terminal
	void WriteGridToFile(); // write grid points to file
	void WriteNearestNeighborToFile(); // write current nearest neighbors to file
	void WriteInterpolationToFile(); // Write current interpolation in F to file
		


	// member variables
	vec xi,yi; // the known gridpoints (xi,yj)
	vec px,py;
	int nx,ny; // dim lengths
	mat F; // the known functionvalues F(xi,yj)
	fxy Fxy; // supplied user function	

	double (BilinearInterpolator::*InterpolatePointStrategy)(fxy,double, double) = &BilinearInterpolator::InterpolatePointSimple;

	NearestPoints nearestpoints;
	
	// for linear system solving strategy
	GS decomposer;
	mat System = zeros<mat>(4,4);
	mat R = zeros<mat>(4,4);
	vec coefficients = zeros<vec>(4);
	vec RHS = zeros<vec>(4);
};
