set terminal aqua

# Set line style 
set style line 1 lc rgb 'blue' pt 11 # circle   
set style line 2 lc rgb 'red' pt 11 # circle   


plot "IterationPoints.txt" using 1:2 with lp,\
"< head -1 IterationPoints.txt" using 1:2 with points ls 1 title 'Initial',\
"< tail -1 IterationPoints.txt" using 1:2 with points ls 2 title 'Final',\
