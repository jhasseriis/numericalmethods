#pragma once

#include <iostream>
#include <armadillo>
#include "../QRDecomposer.hpp"
#include <math.h>
#include <cmath>


using namespace std;
using namespace arma;

class Givens: public QRDecomposer{	
public:

	// Note: the method signatures from the QRDecomposer baseclass is not the same for 
	// all special cases as I initally thought. Instead of refactoring I just wrap the signatures 
	// in the implementation file.
	// 
	// In addition the name 'absdet' is not very good in this case since we actually get sign for the determinant...
	void RHS_QTb_transform(mat& A, vec& b); // transforms RHS:  b -> Q^T*b, A contains thetas in lower triangle

	void decomp(mat& A); // wrapper for decomp(mat& A,mat& R)
	void solve(mat& QR, vec& b,vec& x); // wrapper for solve(mat& Q,mat& R, vec& b, vec& x) 
	void invert(mat& QR, mat& Ainv); // wrapper for invert(mat& Q,mat& R,mat& Ainverse)

	// easy recovery of Q and R for testing purpose
	void RecoverQR(mat& QR,mat& Q, mat& R);
	void RecoverQ(mat& QR,mat& Q);
	void RecoverR(mat& QR,mat& R);

	// decl. of inherited functions
	void decomp(mat& A,mat& R); // the second argument is just to implement the virtual function from the base class
	void solve(mat& Q, mat& R, vec& b, vec& x);
	double absdet(mat& R);
	void invert(mat& Q, mat& R,mat& Ainverse);
	
};
