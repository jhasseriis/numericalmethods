#include <iostream>
#include "GS.hpp"
#include <math.h>

using namespace std;
using namespace arma;

int main(){
	GS gs;

	mat A(3,3); A.zeros();
	
	A <<
	1 << 1 << 2 << endr <<
	0 << 1 << 0 << endr <<
	1 << 5 << 1 << endr;

	mat AOriginal = A;

	mat R(A.n_cols,A.n_cols); R.zeros();

	cout << "A = " << endl << A << endl;
	gs.decomp(A,R);
	cout << "Q = " << endl << A << endl;
	
	double tolerance = pow(10,-12);	
	gs.QRDecomposer::CheckOrtho(A,tolerance);

	cout << "R = " << endl << R << endl;
	cout << "|det(A)| = |det(R)| = " <<	gs.absdet(R) << endl;

	vec x;
	vec b(A.n_cols);
	b(0) = 1;
	b(1) = 2;
	b(2) = 2.2;

	cout << endl << "b =\t" << trans(b) << endl; 
	cout << "Solving system for x... " ;
	gs.solve(A,R,b,x); // solve ARx=b and store solution in x
	cout << "x =\t" << trans(x) << endl;
	cout << "ARx =\t" << trans(A*R*x) << endl;

	cout << "Calculating inverse of A..." << endl;
    mat Ainv(size(A));
	gs.invert(A,R,Ainv);
	cout << "Check: inv(A)*A = " << endl << Ainv*AOriginal; 

	return 0;


}
