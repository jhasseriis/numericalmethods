#include "RootFinder.hpp"

void RootFinder::FindRoot(fs new_functions, vec xstart, double dx, double epsilon){
	SetNewSystem(new_functions,xstart);
	this->dx = dx;
	FindRoot(epsilon);
}

void RootFinder::FindRoot(double epsilon){
	countSteps=0;
	countLoopRuns = 0;
	iterationPoints.clear();
//	cout << "Starting point: x = " << trans(x) ;
	while(norm(fx) > epsilon) // iterate until tolerance is met
	{
//		cout << "norm fx = " << norm(fx) << endl;
		iterationPoints.push_back(x);
		FindNewtonStep(x,dx); 
		BacktrackLinesearch();
	}
	cout << "Found root at (";
	for(int i=0; i < x.n_elem; i++){
	   	cout << x(i);
	    if(i < x.n_elem-1) cout <<	",";
	}
	cout << ") after taking " << countSteps << " steps and " << countLoopRuns << " loop runs. " ;
	cout << "Norm(f(x)) = " << norm(fx) << " < " << epsilon << " (tolerance)"  << endl;
}

void RootFinder::FindNewtonStep(vec& xvec,double dx){
	countSteps++;	
	(*this.*JacobianEvalStrategy)(xvec);
	//JacobianNumericEval(xvec);
	fEval(xvec);
	vec b = -fx; // need minus for solving
	
	mat Q = J; // we create new Q so J is not overwritten when decomp. is called
	mat R(J.n_cols,J.n_cols);
	Solver.decomp(Q,R); // now we have Q and R
	Solver.solve(Q,R,b,NewtonStep);	// solve QR*Deltax = - f(x) and save Deltax -> NewtonStep
}

//////// Linesearch strategies


void RootFinder::BacktrackLinesearch(){
	double lambda = 1;
	vec xPlusLambdaDx;
	double g0,g0prime,gtrial,lambdaTrial,c; // if Quadlinesearch

	while(lambda >= lambdaMinimum){
		countLoopRuns++;

		xPlusLambdaDx = x + lambda*NewtonStep; // Calc current + step

		if( norm(f(xPlusLambdaDx)) < (1-lambda/2.0)*norm(f(x))) break; // break while-loop if condition is ok

		gtrial = 0.5*pow(norm(xPlusLambdaDx),2);
		if(QuadLinesearch && lambda != 1){ // check the != 1 requirement 
			g0	   = 0.5*pow(norm(f(x)),2);
			g0prime = - pow(norm(f(x)),2);
			c = (gtrial-g0-g0prime*lambdaTrial)/pow(lambdaTrial,2);	
			lambdaTrial = lambda;  
			lambda = - g0prime/(2*c);
		}
		else lambdaTrial = lambda ; lambda = lambda/2.0; 
	}	

	// if no suitible lambda is found we take the full step
	if(lambda < lambdaMinimum)	xPlusLambdaDx = x+1.0*NewtonStep;  	// taking the full step seems to work better, so I do that
//	if(lambda < lambdaMinimum)	xPlusLambdaDx = x+lambdaMinimum*NewtonStep;  	
	
	// assign new x and evaluate the new f! 
	x = xPlusLambdaDx;
	fEval(x);	
}

/////// Jacobians ///////
void RootFinder::SetNumericJacobianStrategy(){
	JacobianEvalStrategy = &RootFinder::JacobianNumericEval;
}

void RootFinder::SetAnalyticJacobianStrategy(){
	JacobianEvalStrategy = &RootFinder::JacobianAnalyticEval;
}

void RootFinder::JacobianAnalyticEval(vec& xvec){
	assert(J.n_cols==xvec.n_rows);
	assert(J.n_rows==derivatives.size());
	assert(J.n_cols==derivatives[0].size());
	int n = J.n_rows;
	int m = J.n_cols;

	for(int i=0; i<n; i++){
		for(int k=0; k < m; k++){
			countLoopRuns++;
			J(i,k) = derivatives[i][k](xvec);
		}
	}	
}

void RootFinder::JacobianNumericEval(vec& xvec){
	JacobianNumericEval(J,xvec,dx,functions);
}

void RootFinder::JacobianNumericEval(mat& J,vec& xvec,double dx, fs& functions){
	assert(J.n_cols==xvec.n_rows);
	assert(J.n_rows==functions.size()); // there must be as many rows in J as there are functions
	int n = J.n_rows;
	int m = J.n_cols;
	
	vec xdx = xvec; // vector that is x+dx at the k'th entry

	for(int i=0; i<n; i++){
		for(int k=0; k<m;k++){
			countLoopRuns++;
			xdx(k) += dx; // add dx to k'th variable
			J(i,k) = (functions[i](xdx)-functions[i](xvec))/dx;
			xdx(k) -= dx; // reset the k'th variable
		}
	}
}


// evaluate f(x)
vec RootFinder::f(vec& x){
	vec fx(functions.size());
	for(int i = 0; i < functions.size() ; i++){
		fx(i) = functions[i](x);
	}
	return fx;
}


void RootFinder::fEval(vec& x){
	for(int i = 0; i < functions.size() ; i++){
		fx(i) = functions[i](x);
	}
}

// write to file
void RootFinder::SaveIterationPoints(){
	ofstream file;
	file.open("IterationPoints.txt");
	
	for(int i = 0; i<iterationPoints.size();i++){
		file << iterationPoints.at(i)(0) << "\t" << iterationPoints.at(i)(1) << endl;
	}

	file.close();

}


// Constructors
RootFinder::RootFinder(fs functions):
functions(functions){
	fx = zeros<vec>(functions.size());
	dx = pow(10,-10);
}

RootFinder::RootFinder(fs functions, vec x,double dx):
	dx(dx)
{
	SetNewSystem(functions,x);	
};


// Setups
void RootFinder::SetNewSystem(fs new_functions, vec new_x){
	SetFuncs(new_functions);
	Setx(new_x);
	fEval(new_x);
	SetupJacobian();
}

void RootFinder::SetFuncs(fs new_functions){
	functions = new_functions;	
	fx = zeros<vec>(functions.size());
}

void RootFinder::Setx(vec new_x){
	x = new_x;
}

void RootFinder::SetupJacobian(){
	J = zeros<mat>(functions.size(),x.n_elem);
}

