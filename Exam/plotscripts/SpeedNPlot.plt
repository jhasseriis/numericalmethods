# Speed as function of N plot
# set terminal aqua
set terminal pdf
set output 'output/SpeedAsFunctionOfN.pdf'
set xlabel 'N'
set ylabel 'time [microseconds]'
set key outside

set fit quiet
set fit logfile '/dev/null'
f(x) = a*x**2 + b*x + c
fit f(x) 'output/SpeedFunctionOfN.txt' via a,b,c

plot 'output/SpeedFunctionOfN.txt' title 'Speed as function of N', f(x) title '2nd. order poly fit'
