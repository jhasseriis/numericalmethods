#pragma once

#include <armadillo>
#include <assert.h>


using namespace arma;

double func1(vec& xvec); double dfunc1dx(vec& xvec); double dfunc1dy(vec& xvec);
double func2(vec& xvec); double dfunc2dx(vec& xvec); double dfunc2dy(vec& xvec);

double Rosenbrock(vec& xvec);
double dxRosenbrock(vec& xvec); double dxRosenbrockdx(vec& vec); double dxRosenbrockdy(vec& vec); 
double dyRosenbrock(vec& xvec); double dyRosenbrockdx(vec& vec); double dyRosenbrockdy(vec& vec); 



double dxHimmelblau(vec& xvec);
double dyHimmelblau(vec& xvec);


double dxHarmonicSum(vec& xvec); double dxHarmonicSumdx(vec& xvec); double dxHarmonicSumdy(vec& xvec);
double dyHarmonicSum(vec& xvec); double dyHarmonicSumdx(vec& xvec); double dyHarmonicSumdy(vec& xvec);
