#include "QRDecomposer.hpp"


void QRDecomposer::CheckOrtho(mat& Q,double tolerance){
	int cols = Q.n_cols;
	int rows = Q.n_rows;

	mat O(rows,cols); O.zeros();
	cout << "Columnwise norm:" << endl ;
	for(int i=0; i < cols; i++){
		cout << "i = " << i << ": "  <<norm(Q.col(i)) << endl;
	}
	cout << endl;

	cout << "Matrix elements Oij=dot(Qi,Qj) (tolerance ~ " << tolerance << "): " << endl;
	double product;
	for(int i=0; i< cols; i++){
		for(int j=i; j < cols; j++){
			product = dot(Q.col(i),Q.col(j));
			if(product > tolerance){
				O(i,j) = dot(Q.col(i),Q.col(j)); 
				O(j,i) = dot(Q.col(i),Q.col(j)); 
			}
		}
	}
	cout << "O = " << endl <<  O << endl;
}

// Solve Ax=b for x and stores it in b
void QRDecomposer::BackSub(mat& A, vec& b){
	double element;
	for(int i = b.n_elem-1; i>=0;i--){	
		element = b(i);
		for(int k=i+1; k < b.n_elem ; k++){
		//	cout << "i: " << i << " , k: " << k << endl;
			element = element - A(i,k)*b(k);
		}
		b(i) = element/A(i,i);
	}
}



