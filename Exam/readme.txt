-----------
Preface 
-----------

I am doing assignment 4) and write in C++.
I programmed it on my mac. 
The folder structure is such that builds for mac and linux are seperate. In
the latest commit I have already built both and the results are residing in
the 'output' folder. You can try 'make clean', 'make' if you want. It works on
my mac laptop and my linux desktop. You may have to adjust the include/lib
paths ('-I' and '-L') to point to your installation of the armadillo library.

If you do not want to run the application yourself: you can see the console output log
at 'output/consoleoutput.txt'.

I have implemented two different algorithms to calculate the interpolation
points. I have used a strategy design pattern to accomodate this.
One algorithm is called 'Linear system' and calculates coefficients for f(x,y) ≈ a0 +
a1*x + a2*y + a3*x*y 
The other is more direct and does not require solving a system. See reference.

Reference: https://en.wikipedia.org/wiki/Bilinear_interpolation 

I tried to comment whenever it seemed a good idea. Sorry if I missed something
major.

Note that this program is not optimized, but was created with ease-of-use in
mind. E.g. storing the interpolated F matrix etc. is not really necessary (takes up
space). The problem could have been done without classes, but I feel this is
conceptually nice and generally worth the overhead.

I have tried making the assignments A),B),C) sort of 'natural' in progression
like you often do. 


-----------
Tasks
-----------

A) 6 points:  Implement a method to generate a regular grid (xi,yj). Implement the function
to evaluate the interpolated function value at coordinates (px,py). 

B) 3 points: Implement a method that interpolates now vectors of coordinates (px,py) and
plot the 'surface' of the interpolated points. Compare with the underlying
exact surface. Implement another algorithm for calculating the interpolation.
Compare the computation speeds. 

C) 1 point: Implement a way to handle a general rectilinear grid (ie. can be non-unit
vectors ("cartesian") and not necessarily equidistantly spaced ("regular")).
Visualize the grid and exemplify a nearest neighbor set by generating a random
(x,y) point. Investigate the computation speed of interpolation points with
dimensions N x N as a function of 'N'. 

D) 0 points: Make a proper folder/file structure, add both linux and mac
builds

-----------
Points
-----------

A) was fully completed = 6 points
B) was fully completed = 3 points
C) was fully completed = 1 point
D) was fully completed = 0 points

Total tally = 10 points
