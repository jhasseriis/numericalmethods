#include <iostream>
#include "Jacobi.hpp"
#include <chrono>

using namespace std;
using namespace arma;
using namespace std::chrono;

int main(){
    high_resolution_clock::time_point t1=high_resolution_clock::now(); 
    high_resolution_clock::time_point t2=high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();

	cout << "---------------" << endl;
	Jacobi Jac;

	mat A(3,3); 
	double accuracy;	
	// Example 1
	A <<
	1 << 2 << 4 << endr <<
	2 << 0 << 3 << endr <<
	4 << 3 << 6 << endr;	

	cout << " A = " << endl << A;
	accuracy = pow(10,-12);
	t1 = high_resolution_clock::now();
	Jac.CyclicSweep(A,accuracy);
	t2 = high_resolution_clock::now();
	cout << "Eigenvalues of A:  " << trans(A.diag()) << endl;	

	duration = duration_cast<microseconds>( t2 - t1 ).count();
	cout << "Execution time of example 1 in microsecs: " << duration << endl;

	// Example 2
	cout << "---------------" << endl;
	A <<
	1231 << 0 << 0 << endr <<
	0 << 221 << 0 << endr <<
	0 << 0 << 3 << endr;	


	cout << " A = " << endl << A ;
	accuracy = pow(10,-12);
	t1 = high_resolution_clock::now();
	Jac.CyclicSweep(A,accuracy);
	t2 = high_resolution_clock::now();

	cout << "Eigenvalues of A:  " << trans(A.diag()) << endl;

	duration = duration_cast<microseconds>( t2 - t1 ).count();
	cout << "Execution time of example 2 in microsecs: " << duration << endl;
	// Example 3
	cout << "---------------" << endl;
	A <<
	1 << 2 << 5 << 0 <<  endr <<
	2 << 4 << 9 << 5 <<  endr <<
	5 << 9 << 6 << 1 <<  endr <<	
	0 << 5 << 1 << 5 << endr;

	cout << " A = " << endl << A;
	accuracy = pow(10,-12);
	t1 = high_resolution_clock::now();
	Jac.CyclicSweep(A,accuracy);
	t2 = high_resolution_clock::now();

	cout << "Eigenvalues of A:  " << trans(A.diag()) << endl;
	
	duration = duration_cast<microseconds>( t2 - t1 ).count();
	cout << "Execution time of example 3 in microsecs: " << duration << endl;
	cout << endl;


	return 0;
}
