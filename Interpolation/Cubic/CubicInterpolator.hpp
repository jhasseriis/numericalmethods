#pragma once

#include <armadillo>
#include <math.h>
#include <fstream>

using namespace arma;
using namespace std;

class CubicInterpolator{
public:
	
	// Variables
	vec xi;
	vec yi;
	vec pi;
	vec ci;
	vec bi; // for convenience when diff / integ
	vec di;

	vec SiIntegrals; // the integrals of the Si spline segment
	
	string fileWriteName = "CubicInterpolator.txt";
	
	// Constructors and destructors
	CubicInterpolator(vec xi,vec yi);
	~CubicInterpolator();

	// Member functions
	double evaluate(double z,string type); // type can be empty (just the normal spline), or it can be "Derivative", or "Integrate")
	double derivative(double z);
	double integrate(double z);
	vec splitArray(vec array,int lowerIndex,int upperIndex);
	int findIndex(vec array,double value);
	void exportToTxt(vec x); // for arbitrary x vector, produce text output
};
