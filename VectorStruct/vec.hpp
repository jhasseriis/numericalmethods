#include <iostream>
#include <vector>

namespace MyVectorClass{

	
class vec{
public:
	int size;
	std::vector<double> data;
	vec(int n);

	// methods
	void set(int i,double value);
	double get(int n);
	double& operator[](int i);
	double& operator()(int i);
};


}
