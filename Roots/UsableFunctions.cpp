#include "UsableFunctions.hpp"

// func1 and func2 + derivs

double func1(vec& xvec){
	double A = 10000.0;
	return A*xvec(0)*xvec(1)-1; // A*x*y = 1
}

double func2(vec& xvec){
	double A = 10000.0;
	double result = exp(-xvec(0))+exp(-xvec(1))-1-1.0/A;
	return result;
}

double dfunc1dx(vec& xvec){
	double A = 10000.0;
	return A*xvec(1);
}

double dfunc1dy(vec& xvec){
	double A = 10000.0;
	return A*xvec(0);
}

double dfunc2dx(vec& xvec){
	double A = 10000.0;
	double result = -exp(-xvec(0));
	return result;
}
double dfunc2dy(vec& xvec){
	double A = 10000.0;
	double result = -exp(-xvec(1));
	return result;
}
// Rosenbrock

double Rosenbrock(vec& xvec){
	assert(xvec.n_elem>=2);
	double result,x,y;
	x = xvec(0);
	y = xvec(1);
	result = pow(1-x,2)+100*pow(y-pow(x,2),2);
	return result;
}


double dxRosenbrock(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = -2.0*(1.0-x)-400.0*(y-pow(x,2))*x;
	return result;
}


double dyRosenbrock(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 200.0*(y-pow(x,2));
	return result;
}


double dxRosenbrockdx(vec& xvec){return 2-400.0*(xvec(1)-pow(xvec(0),2))+800.0*pow(xvec(0),2); }
double dxRosenbrockdy(vec& xvec){return -400.0*xvec(0);}
double dyRosenbrockdx(vec& xvec){return -400.0*xvec(0);}
double dyRosenbrockdy(vec& xvec){return 200.0;}

// himmelblau gradient
double dxHimmelblau(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 4.0*x*(pow(x,2)+y-11)+2*(x+pow(y,2)-7);
	return result;
}

double dyHimmelblau(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 2*(pow(x,2)+y-11)+4*y*(x+pow(y,2)-7);
	return result;
}

// harmonic sum: f(x,y) = 4*(x-3)^2 + 2*(y+2)^2
double dxHarmonicSum(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 8*(x-3);
	return result;
}

double dyHarmonicSum(vec& xvec){
	double x = xvec(0);
	double y = xvec(1);
	double result = 4*(y+2);
	return result;
}

double dxHarmonicSumdx(vec& xvec){return 8;} double dxHarmonicSumdy(vec& xvec){return 0;}
double dyHarmonicSumdx(vec& xvec){return 0;} double dyHarmonicSumdy(vec& xvec){return 4;}
