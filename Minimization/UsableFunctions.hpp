#pragma once

#include <armadillo>
#include <assert.h>


using namespace arma;
using f = std::function<double(vec&)>;
using fi = std::vector<f>;
using fij = std::vector<fi>;


double Rosenbrock(vec& xvec);
double dxRosenbrock(vec& xvec), dyRosenbrock(vec& vec);
double dxxRosenbrock(vec& xvec), dxyRosenbrock(vec& xvec),
	   dyxRosenbrock(vec& xvec), dyyRosenbrock(vec& xvec);
fi RosenbrockGrad(); // return gradient 
fij RosenbrockHessian(); // return hessian

double Himmelblau(vec& xvec);
double dxHimmelblau(vec& xvec), dyHimmelblau(vec& vec);
double dxxHimmelblau(vec& xvec), dxyHimmelblau(vec& xvec),
	   dyxHimmelblau(vec& xvec), dyyHimmelblau(vec& xvec);



fi HimmelblauGrad(); // return gradient 
fij HimmelblauHessian(); // return hessian



fi HarmonicGrad();
double Harmonic(vec& xvec);
double dxHarmonic(vec& xvec); double dyHarmonic(vec& xvec);
