#include "GS.hpp"

void GS::decomp(mat& A,mat& R){
	// We do in-place transformations, i.e. we turn A->Q
	int cols = A.n_cols;
	vec ai;
	for(int i=0; i < cols;i++){

	//	cout << "i: " <<  i << endl;
		ai = A.col(i); // extract column	
		R(i,i) = norm(ai); // diagonal elements of R
		A.col(i) = ai/norm(ai); // change into Q
		for(int j=i+1; j < cols; j++){
	//		cout << "j: " <<  j << endl;
			R(i,j) = dot(A.col(i),A.col(j)); // Rij = qi^T*aj
			A.col(j) = A.col(j)-A.col(i)*R(i,j); // aj = aj-qi*Rij	
		}
	} 
}


// backsubstitution to solve QRx = b
void GS::solve(mat& Q, mat& R,vec& b, vec& x){
	//QRx=b -> Rx = Q^Tb
	x = trans(Q)*b;
	GS::QRDecomposer::BackSub(R,x);
}


// for GS the |det(A)|=|det(R)| = prod(Rii)
double GS::absdet(mat& R){
	double absdet=1;

	for(int i=0; i < R.n_cols ; i++){
		absdet *=R(i,i);	
	}
	absdet = abs(absdet);
	
	return absdet;
}

void GS::invert(mat& Q,mat& R, mat& Ainverse){
	vec ei(Ainverse.n_rows); ei.zeros();
	vec Ainvi(Ainverse.n_rows);

	for(int i=0;i<Q.n_cols;i++)
	{
		ei(i) = 1;
		Ainvi = Ainverse.col(i);
		GS::solve(Q,R,ei,Ainvi);
		Ainverse.col(i) = Ainvi;
		ei.zeros();
	}	
}
