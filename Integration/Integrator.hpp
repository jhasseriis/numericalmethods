#pragma once
#include <iostream>
#include <armadillo>
#include <cmath>
#include <functional>

using namespace arma;
using namespace std;

using f = std::function<double(double)>;

class Integrator{
public:	

	Integrator();
	double Adapt_Open(f initialIntegrand,double a, double b, double acc, double eps);

	double Adapt24_Open(f initialIntegrand, double a, double b, double acc, double eps, double f2, double f3);
	
	double Adapt_Open_2dim(std::function<double(vec)>,vec a,vec b,double acc,double eps);


	double acc = 1e-4;
	double eps = 1e-4;
//	f integrand;
//	f initialIntegrand;

	double Adapt_Closed(f initialIntegrand,double a, double b, double acc, double eps);

	double Adapt12_Closed(f initialIntegrand,double a, double b, double acc, double eps, double f0,double f2);
	
	int n_calls = 0;

	
	double ClenshawCurtis(f initialIntegrand, double a,double b, double acc, double eps);

};
