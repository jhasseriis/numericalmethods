#include <iostream>
#include "Steppers.hpp"
#include "Driver.hpp"
#include <armadillo>

using namespace arma;
using namespace std;


void oscillator(double t, vec& y, vec& dydt){
	double omega = 1; // frequency
	double gamma = 0.51; // damping
	double alpha = 0.11; // driving force

	dydt[0] = y[1];
	dydt[1] = -omega*omega*y[0]  - gamma*y[1] + alpha*cos(t);
}

int main(){

	int n = 2;

	vec y(n);  
/*	vec dydt(n);
	vec err(n);
	vec yh(n);
*/
	y(0) = 1; y(1) = 0;
	double a = 0;
	double b = 100;
	double t = 0;
	double h = 0.01;
	double acc = 1e-4;
	double eps = 1e-4;	
	cout << "---------------------------" << endl;
	cout << "Driven damped oscillator: x'' = -omega^2*x - gamma*x' + alpha*cos(t) " << endl;

	cout << endl << "Solving system using rkstep12 embedded " << endl;
	Driver d(a,b,h,acc,eps,y,oscillator,rkstep12);
	d.Solve();
	d.WritePath();

	cout << endl << "Solving system using rkstep2 non-embedded " << endl;
	Driver d2(a,b,h,acc,eps,y,oscillator,rkstep2_non_embedded);
	d2.Solve();
	d2.WritePath();

	cout << endl << "see gnuplot plot for plot........ change terminal in gnuplot script if you don't have aquaterm" << endl;
	
	cout << endl;
	

	return 0;
}
