#include "vec.hpp"

namespace MyVectorClass{

vec::vec(int n):
size(n),data(n) 	{}

void vec::set(int i,double value){
	data[i] = value;
}

double vec::get(int n){
	return data[n];
}


double& vec::operator[](int i){
	return data[i];
}


double& vec::operator()(int i){
	return data[i];
}




}
