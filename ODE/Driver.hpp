#pragma once
#include <iostream>
#include <armadillo>
#include <fstream>


using namespace arma;
using namespace std;


class Driver{
public:
	Driver(){};
	Driver(double a, double b, double h0, double acc, double eps, vec y0,  void f(double t, vec& y, vec& dydt),
			void stepper(double t,double h, vec& y, void f(double t, vec& y,vec& dydt),vec& yh,vec& err,int* n_evals));

	// functions
	void Solve(); // solve from t=a -> b
	void Drive(); // drives a single step (wrapper for stepper)
	void AdaptSize(); // adapts the step size

	void PrintPath();
	void WritePath();

	// variables
	int n;
	int n_evals;
	double a,b; // from a to b
	double t=0,h = 0.01; // current point and stepsize
	double acc,eps; // absolute and relative error
	double ti,ei; // local errors
	double normy;
	vec y;
	vec err;
	vec yh;
	vec dydt;
	

	// for saving path
	std::vector<vec> ys;
	std::vector<double> ts;

	// Pointer to function dydt
	void (*f)(double t, vec& y, vec& dydt);	
	// Pointer to stepper function:
	void (*stepper)(double t,double h, vec& y, void f(double t, vec& y,vec& dydt),vec& yh,vec& err,int* n_evals);
};
