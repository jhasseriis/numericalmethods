#pragma once

#include <armadillo>
#include <iostream>
#include <cmath>
#include "GramSchmidt/GS.hpp"
//#include <gnuplot-iostream.h>
#include <fstream>

using namespace std;
using namespace arma;

using fi = std::function<double(double x)>; // function reference alias
using fN = std::vector<fi>; // vector of function references alias

class LeastSquareFitter{
public:
	LeastSquareFitter(fN& functionList,vec& x,vec& y, vec& dy);	// construct leastsquarefitter

	LeastSquareFitter();

	void evalCoeffs(fN& functionList, vec& x, vec& y, vec& dy); // takes x,y,dy experimental data 

	void evalFit(); // evaluates fit
	void evalFit(double xmin, double xmax, int xsteps);	

	void fitSave(); // saves fit in 2 blocks: First block has the experimental data, the second block has results (xfit,yfit,yfit+dc,yfit-dc)
	void fitSave(double xmin, double xmax, int xsteps);	
	

	vec x,y,dy,coeffs,dcoeffs;
	vec xfit, yfit,yfitPlusdc,yfitMinusdc;
	mat covariance;
	fN functionList;
	GS decomposer;

};
