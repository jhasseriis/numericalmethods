#include <iostream>
#include "vec.hpp"

using namespace MyVectorClass;
using namespace std;

int main(){
	vec myVector(3);
	
	cout << endl <<  "Setting vector v=(5,2,6)" << endl << endl;
	myVector.set(0,5);
	myVector.set(1,2);
	myVector.set(2,6);

	cout << "Printing vector elements with get(i) " << endl;
	for(int i=0;i < myVector.size ; i++){
		cout << myVector.get(i) << endl;
	}

	cout << endl;

	cout << "Printing vector elements with operator[i] " << endl;
	for(int i=0;i < myVector.size ; i++){
		cout << myVector[i] << endl;
	}

	cout << endl;

	cout << "Printing vector elements with operator(i) " << endl;
	for(int i=0;i < myVector.size ; i++){
		cout << myVector(i) << endl;
	}


	return 0;
}
