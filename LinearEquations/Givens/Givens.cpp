#include "Givens.hpp"



void Givens::decomp(mat& A){
	decomp(A,A); // second argument is never used
}

void Givens::RHS_QTb_transform(mat& A,vec& b){
	double theta, bq, bp;
	int cols = A.n_cols;
	int rows = A.n_rows;
	
	for(int q=0; q < cols; q++){
		for(int p=q+1; p < rows; p++){
			theta = A(p,q);
			bp = b(p); 
			bq = b(q);
			b(q) =  bq*cos(theta) + bp*sin(theta);			
			b(p) = -bq*sin(theta) + bp*cos(theta);			
		}
	}	
}

void Givens::solve(mat& QR,vec& b,vec& x){
	solve(QR,QR,b,x); 
}

void Givens::invert(mat& QR, mat& Ainverse){
	invert(QR,QR,Ainverse);
}

void Givens::RecoverQR(mat& QR,mat& Q,mat&R){
	RecoverQ(QR,Q);
	RecoverR(QR,R);
}

void Givens::RecoverQ(mat& QR, mat& Q){
	int cols = QR.n_cols;
	int rows = QR.n_rows;

	vec ei(rows);
	ei.zeros();
	for(int i=0; i < rows ; i++){
		ei(i) = 1;
		RHS_QTb_transform(QR,ei);
		for(int j=0; j < cols;j++){
			Q(i,j) = ei(j);
		}
		ei.zeros();
	}
}

void Givens::RecoverR(mat& QR,mat& R){
	for(int i=0; i < R.n_rows ; i++){
		for(int j=i; j < R.n_cols ; j++){
			R(i,j) = QR(i,j);	
		}
	}
}
// Implementation of inherited virtual functions
void Givens::decomp(mat& A,mat& R_dummy){
	double theta, xq, xp;
	int cols = A.n_cols;
	int rows = A.n_rows;
	for(int q=0; q < cols ; q++){
		for(int p=q+2; p < rows; p++){
	//		cout << "(q = " << q << ", p = " << p << ")" << endl;
			theta = atan2(A(p,q),A(q,q));
			for(int k=q; k < cols; k++){
				xq = A(q,k);
				xp = A(p,k);
				A(q,k) =  xq*cos(theta) + xp*sin(theta); 
				A(p,k) = -xq*sin(theta) + xp*cos(theta);
				
				A(p,q) = theta; // Store angle in zeroed elements
			}
		}
	}
}


// this method ended up being a little messy..
void Givens::solve(mat& QR, mat& R_dummy, vec& b, vec& x){
	// solves QRx=b for x
	vec tempVector = b; // temp. storage of b
	RHS_QTb_transform(QR,b); // b-> Q^T b	

	// Solve Rx = b 
	QRDecomposer::BackSub(QR,b);

	x = b; // this is x
	b = tempVector; // reset b to its initial values
}

double Givens::absdet(mat& R){
	double result = 1;
	for(int i=0;i < R.n_cols ; i++){
		result *= R(i,i);
	}
	return result;
}

void Givens::invert(mat& QR,mat& R_dummy, mat& Ainverse){
	vec ei(Ainverse.n_rows); ei.zeros();
	vec Ainvi;

	for(int i=0;i<QR.n_cols;i++)
	{
		ei(i) = 1;
		solve(QR,ei,Ainvi);
		Ainverse.col(i) = Ainvi;
		ei.zeros();
		Ainvi.zeros();
	}	
}

