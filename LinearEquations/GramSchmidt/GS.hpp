#include <armadillo>
#include <iostream>
#include "../QRDecomposer.hpp"
#include <math.h>
#include <cmath>

using namespace arma;
using namespace std;

class GS: public QRDecomposer {

public:
	// A(n,m), Q(n,m), R(m,m)
	void decomp(mat& A,mat& R); // decomposes A=QR and lets Q->A and R->R
	void solve(mat& Q, mat& R, vec& b, vec& x); // Solves QRx=b for x
	double absdet(mat& R);
	void invert(mat& Q,mat& R, mat& Ainverse); // inverts A by its QR decomposition
};
