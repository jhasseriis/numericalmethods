set term aqua font 'helvetica bold,15'
set key outside
set style line 1 lc rgb '#0060ad' lt 1 lw 2 # --- blue
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 7 # --- red
plot 'fitResults.txt' index 0 using 1:2:3 with errorbars ls 1 title 'data',\
'fitResults.txt' index 1 u 1:2 w lines ls 1 title 'fit',\
'fitResults.txt' index 1 u 1:3 w lines ls 2 title 'fit+dc',\
'fitResults.txt' index 1 u 1:4 w lines ls 2 title 'fit-dc'

