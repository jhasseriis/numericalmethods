#include <iostream>
#include <armadillo>
#include "BilinearInterpolator.hpp"
#include <chrono>
#include <fstream>
#include <cstdio>

using namespace std;
using namespace arma;
using namespace std::chrono;

using fxy = std::function<double (double,double)>; // function of two vars f(x,y)
using TIME = high_resolution_clock;

double harmonic(double x,double y){
	return x*x+y*y;
}
double linear1d(double x,double y){
	return x;
}
double linear2d(double x,double y){
	return x+y;
}

double sinxgaussy(double x,double y){
	return sin(x)*exp(-y*y);
}

int main(){
//	freopen("output/consoleoutput.txt","w",stdout);

	cout << endl;
	string BREAK = "------------------------------------------------";		
	cout << BREAK << endl;
	// A) Single point interpolation
	cout << "A): Single point interpolation" << endl << endl;
	BilinearInterpolator BI;
	BI.UseSimpleStrategy();
	vec xlim(2),ylim(2); int nx = 11, ny=11;
	xlim << -5 << 5;
	ylim << -5 << 5;

	cout << "Generating grid..." << endl;
	BI.GenerateGrid(xlim,nx,ylim,ny);
	cout << "Grid xvals: " << BI.xi.t();
	cout << "Grid yvals: " << BI.yi.t();
	cout << endl << "Interpolating point p=(-0.2,1.75) for f(x,y) = x + y for current grid: f(-0.2,1.75) = " <<  (BI.*(BI.InterpolatePointStrategy))(linear2d,-0.2,1.75) << "\t (actual value: " << linear2d(-0.2,1.75) << ")"; 
	cout << endl << "Interpolating point p=(3.1,2.1) for f(x,y) = x^2 + y^2 for current grid: f(3.1,2.1) = " <<  (BI.*(BI.InterpolatePointStrategy))(harmonic,3.1,2.1) << "\t (actual value: " << harmonic(3.1,2.1) << ")" << endl;
	
	cout << endl << "The 2d linear function is ofcourse exact. The interpolation of the harmonic functions would get better with better grid granularity! " << endl;
	
	cout << BREAK << endl;

	// B) Surface plots and speed comparison
	cout << "B) Surface plots: Interpolation of function sin(x)*exp(-x^2) and speed comparison" << endl << endl;
	TIME::time_point t_start,t_end; // init timers
//	xlim(0) = -3; xlim(1) = 3; ylim(0) = -3; ylim(1) = 3;
	nx = 13; ny = 13; // new granularity
	BI.GenerateGrid(xlim,nx,ylim,ny);	
	cout << "Readjusting grid granularity with nx = " << nx << " and ny = " << ny << endl;
	int nx_interp = 3*nx; int ny_interp = 3*ny;
	vec px,py; // points to interpolate
	px = linspace<vec>(xlim(0),xlim(1),nx_interp); 
	py = linspace<vec>(ylim(0),ylim(1),ny_interp);
    cout << "Interpolation points: " 
		 << "\tx:[" << px(0) << ";" << px(px.n_elem-1) << "] in nx_interp = " << nx_interp << " points"
		 << "\ty:[" << py(0) << ";" << py(py.n_elem-1) << "] in ny_interp = " << ny_interp << " points" << endl; 	
	cout << "inputting points to be interpolated ... " << endl << endl;
	cout << "Interpolating and saving to .txt. Run 'make plot' to plot" << endl;

	// calculate and time with simple strategy
	t_start = TIME::now();
	BI.InterpolateGrid(sinxgaussy,px,py);
	t_end = TIME::now();
	BI.WriteInterpolationToFile();
	auto executionspeed1 = duration_cast<microseconds>(t_end-t_start).count();
	cout << endl << "Execution speed of 'simple' strategy: " << executionspeed1 << " microseconds = " << executionspeed1*1e-3 << " milliseconds = " << executionspeed1*1e-6 << " seconds" << endl << endl;

	// calculate and time with linear system strategy	
	BI.UseLinearSystemStrategy();	
	t_start = TIME::now();
	BI.InterpolateGrid(sinxgaussy,px,py);
	t_end = TIME::now();
	auto executionspeed2 = duration_cast<microseconds>(t_end-t_start).count();
	cout <<  "Execution speed of 'linear system' strategy: " << executionspeed2 << " microseconds = " << executionspeed2*1e-3 << " milliseconds = " << executionspeed2*1e-6 << " seconds" << endl << endl;
	cout << "Speed(Simple) / Speed(Linear System) ≈ " << (double) executionspeed1/(double) executionspeed2 << endl;
	cout << "The 'simple' strategy should be much faster since the 'linear system' strategy needs to QR decompose and solve the system, while the 'simple' strategy immediately calculates the result!" << endl; 

	// C) Plotting the grid, random nearest neighbor and adding general rectilinear grids. 
	cout << BREAK << endl;
	cout << "C) Generate and plot general rectilinear and find nearest neighbor for random point. Computation speed as function of N" << endl << endl;
	BI.UseSimpleStrategy();
	cout << "Setting new grid: now general rectilinear" << endl;

	// generate rectilinear grid
	nx = 5; ny = 8;
	vec xgrid(nx), ygrid(ny);
	xgrid << 0 << 0.55 << 0.65 << 2.1 << 4.1;
	ygrid << 0 << 0.25 << 2.1 << 3.4 << 5 << 5.2 << 6 << 9; 
	xlim(0) = xgrid(0); xlim(1) = xgrid(xgrid.n_elem-1);
	ylim(0) = ygrid(0); ylim(1) = ygrid(ygrid.n_elem-1);
	BI.SetGrid(xgrid,ygrid);
	BI.WriteGridToFile();

	cout << "Grid xvals: " << BI.xi.t();
	cout << "Grid yvals: " << BI.yi.t();

	cout << endl <<"Generating random point in interpolation region and find the nearest neighbors: ";
	// generate random point inside interp. region 
	arma_rng::set_seed_random();
	vec a(2); a(0) = xlim(0); a(1) = ylim(0);
	vec b(2); b(0) = xlim(1); b(1) = ylim(1);
	vec rndVec = randu<vec>(2);
	vec randompoint = a + rndVec%(b-a);

	cout << "(" << randompoint[0] << "," << randompoint(1) << ")" << endl;
	// find the neighbors and save
	cout << "Finding nearest neighbors and save to file. Run 'make plot' to plot" << endl << endl;
	BI.FindNearestPoints(randompoint(0),randompoint(1));
	BI.WriteNearestNeighborToFile();

	// investigate computation time as a function of N
	cout << "Now we investigate the computation time of NxN points..." << endl;
	ofstream myfile;
	myfile.open("output/SpeedFunctionOfN.txt");

	xlim << -3 << 3; ylim << -3 << 3;
	nx = 7; ny = 7;
	BI.GenerateGrid(xlim,nx,ylim,ny);

	cout << "Grid xvals: " << BI.xi.t();
	cout << "Grid yvals: " << BI.yi.t();


	double mean; int runs = 3;
	cout << "Calculating computation speed from nx_inter = ny_interp = 1 to 250, by averaging 3 runs... be patient!" << endl; 
	cout << endl << "nx_interp progress: " << endl;
	for(ny_interp = 1; ny_interp <= 250; ny_interp++){
		px = linspace<vec>(xlim(0),xlim(1),ny_interp); 
		py = linspace<vec>(ylim(0),ylim(1),ny_interp);
		mean = 0;
		if(ny_interp%25 == 0) cout << ny_interp << endl;
		for(int i = 0; i < runs; i++){
			t_start = TIME::now();
			BI.InterpolateGrid(sinxgaussy,px,py);
			t_end = TIME::now();
			mean += (double) duration_cast<microseconds>(t_end-t_start).count();
		}
	    myfile <<  mean/((double) runs) << endl;
	}
	myfile.close();
	cout << BREAK << endl;
	return 0;
}
