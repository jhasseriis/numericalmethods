#pragma once

#include <armadillo>
#include <iostream>
#include "../LinearEquations/GramSchmidt/GS.hpp"

using namespace arma;
using namespace std;

using f = std::function<double(vec&)>; // function of n variables
using fi = std::vector<f>; // vector of functions
using fij = std::vector<fi>; // 'matrix' of functions

class Minimizer{
public:
			
	Minimizer(f function, fi gradientFuncs, fij hessianFuncs,vec xInitial);
	Minimizer(){};

	void Minimize();
	void Backtrack();
	void FindStepInPlace();
	void FindStepBroydenInPlace();
	vec CalculateGrad(vec& x);	
	void CalculateGradInPlace();
	void CalculateHessianInverseInPlace();
	void BroydensUpdate();

	void SetSystem(f function, fi gradientFuncs, fij hessianFuncs, vec xInitial);

	void SetHessianInverseStrategyToBroyden();
	void SetHessianInverseStrategyToNewton();

	GS solver;
	int n;
	vec x;
	vec dx;
	vec gradient;
	vec y;
	mat hessianInverse;
	mat hessian;
	mat auxillaryMatrix; // matrix used for auxillary operations

	bool firstHessianEvaluation = true;
	double alpha = pow(10,-1);
	double lambdaMin = 1.0/128.0;
	double accuracy = pow(10,-3);
	void  (Minimizer::*HessianInverseStrategy)() = &Minimizer::CalculateHessianInverseInPlace;
	f function;
	fi gradientFuncs;
	fij hessianFuncs;
};
